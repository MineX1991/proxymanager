package de.MineX1991.ProxyManager.Commands;

import de.MineX1991.ProxyManager.Main.Data;
import de.MineX1991.ProxyManager.Manager.BanManager;
import java.util.List;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class BanList
  extends Command
{
  public BanList()
  {
    super("banlist");
  }
  
  public void execute(CommandSender sender, String[] args)
  {
    if (sender.hasPermission("bungeecord.command.banlist") || sender.hasPermission("bungeecord.*"))
    {
      if (BanManager.getBannedPlayers().size() == 0)
      {
        sender.sendMessage(String.valueOf(Data.prefix) + "§cDerzeit sind keine Spieler gebannt...");
        return;
      }
      sender.sendMessage(String.valueOf(Data.prefix) + "§6Folgende Spieler sind gebannt!");
      for (String x : BanManager.getBannedPlayers()) {
        sender.sendMessage(String.valueOf(Data.prefix) + "§c" + x + " §8| §7/check§c " + x);
      }
      sender.sendMessage("");
      sender.sendMessage(Data.prefix + "§6Gebannte Spieler: §3" + BanManager.getBannedPlayers().size());
    }
    else
    {
      sender.sendMessage(String.valueOf(Data.prefix) + "§cDir fehlt dafür die Berechtigung!");
    }
  }
}
