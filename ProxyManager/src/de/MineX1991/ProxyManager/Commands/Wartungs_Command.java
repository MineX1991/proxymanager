package de.MineX1991.ProxyManager.Commands;

import de.MineX1991.ProxyManager.MOTD.Manager_Chat;
import de.MineX1991.ProxyManager.Main.Data;
import java.util.concurrent.TimeUnit;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import de.MineX1991.ProxyManager.Main.Main;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.scheduler.TaskScheduler;

public class Wartungs_Command
  extends Command
{
  public Wartungs_Command(String string)
  {
    super(string);
  }
  
  public void execute(CommandSender arg0, String[] arg1)
  {
    ProxiedPlayer p = (ProxiedPlayer)arg0;
    if (p.hasPermission("bungeecord.command.wartungen") || p.hasPermission("bungeecord.*"))
    {
      if (!Manager_Chat.Wartungen.booleanValue())
      {
        if (arg1.length == 0)
        {
          BungeeCord.getInstance().broadcast(String.valueOf(Data.prefix) + "§cTryRusher.de §7wird in §ceiner Minute §7in den §cWartungsmodus §7versetzt.");
          ProxyServer.getInstance().getScheduler().schedule(Main.plugin, new Runnable()
          {
            public void run()
            {
              BungeeCord.getInstance().broadcast(String.valueOf(Data.prefix) + "§cTryRusher.de §7wird in §c30 Sekunden §7in den §cWartungsmodus §7versetzt.");
            }
          }, 30L, TimeUnit.SECONDS);
          ProxyServer.getInstance().getScheduler().schedule(Main.plugin, new Runnable()
          {
            public void run()
            {
              BungeeCord.getInstance().broadcast(String.valueOf(Data.prefix) + "§cTryRusher.de §7wird in §c10 Sekunden §7in den §cWartungsmodus §7versetzt.");
            }
          }, 50L, TimeUnit.SECONDS);
          ProxyServer.getInstance().getScheduler().schedule(Main.plugin, new Runnable()
          {
            public void run()
            {
              BungeeCord.getInstance().broadcast(String.valueOf(Data.prefix) + "§cTryRusher.de §7wird in §c5 Sekunden §7in den §cWartungsmodus §7versetzt.");
            }
          }, 55L, TimeUnit.SECONDS);
          ProxyServer.getInstance().getScheduler().schedule(Main.plugin, new Runnable()
          {
            public void run()
            {
              Manager_Chat.Wartungen = Boolean.valueOf(true);
              ProxyServer.getInstance().broadcast(String.valueOf(Data.prefix) + "§cTryRusher.de §7befindet sich §cnun §7im §cWartungsmodus§7.");
              for (ProxiedPlayer p : ProxyServer.getInstance().getPlayers()) {
                if (!p.hasPermission("bungeecord.wartungen.join")) {
                  p.disconnect(String.valueOf(Data.prefix) + "§cTryRusher.de §7befindet sich nun im §cWartungsmodus. \n §7Das §cBetreten §7des §cNetzwerkes§7 ist derzeit nicht möglich.");
                }
              }
            }
          }, 60L, TimeUnit.SECONDS);
        }
        else if ((arg1.length == 1) && (arg1[0].equalsIgnoreCase("now")))
        {
          Manager_Chat.Wartungen = Boolean.valueOf(true);
          ProxyServer.getInstance().broadcast(String.valueOf(Data.prefix) + "§6Wartungsmodus wurde aktiviert.");
          for (ProxiedPlayer p1 : ProxyServer.getInstance().getPlayers()) {
            if (!p1.hasPermission("bungeecord.wartungen.join")) {
              p1.disconnect(String.valueOf(Data.prefix) + "§cTryRusher.de §7befindet sich nun im §cWartungsmodus. \n §7Das §cBetreten §7des §cNetzwerkes§7 ist derzeit nicht möglich.");
            }
          }
        }
      }
      else
      {
        Manager_Chat.Wartungen = Boolean.valueOf(false);
        ProxyServer.getInstance().broadcast(String.valueOf(Data.prefix) + "§6Wartungsmodus wurde deaktiviert.");
      }
    }
    else {
      p.sendMessage("§cKeine Rechte.");
    }
  }
}
