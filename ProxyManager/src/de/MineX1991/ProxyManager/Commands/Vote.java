package de.MineX1991.ProxyManager.Commands;

import de.MineX1991.ProxyManager.Main.Data;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class Vote
  extends Command
{
  public Vote()
  {
    super("premium");
  }
  
  public void execute(CommandSender sender, String[] args)
  {
    ProxiedPlayer p = (ProxiedPlayer)sender;
    p.sendMessage(String.valueOf(Data.prefix) + "§7Du möchtest den §cServer §7unterstützen? Dann kaufe dir doch §cPremium§7!");
  }
}
