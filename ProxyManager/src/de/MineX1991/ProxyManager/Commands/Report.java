package de.MineX1991.ProxyManager.Commands;

import java.util.ArrayList;
import java.util.HashMap;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.plugin.Command;

public class Report
  extends Command
{
  public static HashMap<ProxiedPlayer, String> reported = new HashMap();
  public static HashMap<ProxiedPlayer, ProxiedPlayer> reporter = new HashMap();
  public static ArrayList<ProxiedPlayer> reportee = new ArrayList();
  public static String Prefix = "§8[§eReport§8] ";
  
  public Report()
  {
    super("report");
  }
  
  public void execute(CommandSender sender, String[] args)
  {
    ProxiedPlayer p = (ProxiedPlayer)sender;
    if (args.length < 2)
    {
      sender.sendMessage(Prefix + "§7Verwende /report <Name> <Hacks, Beleidigungen, Teaming, Werbung, Sonstiges>");
    }
    else
    {
      if (ProxyServer.getInstance().getPlayer(args[0]) == null)
      {
        sender.sendMessage(Prefix + "§7Dieser §eSpieler §7ist nicht Online.");
        return;
      }
      if (!reportee.contains(p))
      {
        if (args[1].equalsIgnoreCase("hacks"))
        {
          reportee.add(p);
          reporter.put(ProxyServer.getInstance().getPlayer(args[0]), p);
          p.sendMessage(Prefix + "§7Vielen Dank für deinen §eReport§7.");
          reported.put(ProxyServer.getInstance().getPlayer(args[0]), args[1]);
          for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
            if (all.hasPermission("bungeecord.command.kick") || all.hasPermission("bungeecord.*"))
            {
              TextComponent msg2 = new TextComponent(Prefix + "§7Klicke §e§lHIER§7 um den Report zu bearbeiten!");
              ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
              
              msg2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cVerbinden").create()));
              msg2.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/r " + args[0]));
              all.sendMessage("§3§m§l---------------------------------");
              all.sendMessage(Prefix + "§7Der Spieler §e" + p.getName() + "§7 hat einen §eReport §7erstellt.");
              all.sendMessage(Prefix + "§7Gemeldeter Spieler: §e" + args[0]);
              all.sendMessage(Prefix + "§7Angegebener Grund: §e" + args[1]);
              all.sendMessage(Prefix + "§7Server: §e" + ProxyServer.getInstance().getPlayer(args[0]).getServer().getInfo().getName());
              all.sendMessage(Prefix + "§7Ping: §e" + target.getPing());
              all.sendMessage(Prefix + "§7Nutze §e/r " + args[0] + "§7 um diesen zu bearbeiten.");
              all.sendMessage(msg2);
              all.sendMessage("§3§m§l---------------------------------");
            }
          }
        }
        else if (args[1].equalsIgnoreCase("beleidigungen"))
        {
          reporter.put(ProxyServer.getInstance().getPlayer(args[0]), p);
          reportee.add(p);
          p.sendMessage(Prefix + "§7Vielen Dank für deinen §eReport§7.");
          reported.put(ProxyServer.getInstance().getPlayer(args[0]), args[1]);
          for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
            if (all.hasPermission("bungeecord.command.kick") || all.hasPermission("bungeecord.*"))
            {
              ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
              
              TextComponent msg22 = new TextComponent(Prefix + "§7Klicke §e§lHIER§7 um den Report zu bearbeiten!");
              msg22.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cVerbinden").create()));
              msg22.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/r " + args[0]));
              all.sendMessage("§3§m§l---------------------------------");
              all.sendMessage(Prefix + "§7Der Spieler §e" + p.getName() + "§7 hat einen §eReport §7erstellt.");
              all.sendMessage(Prefix + "§7Gemeldeter Spieler: §e" + args[0]);
              all.sendMessage(Prefix + "§7Angegebener Grund: §e" + args[1]);
              all.sendMessage(Prefix + "§7Server: §e" + ProxyServer.getInstance().getPlayer(args[0]).getServer().getInfo().getName());
              all.sendMessage(Prefix + "§7Ping: §e" + target.getPing());
              all.sendMessage(Prefix + "§7Nutze §e/r " + args[0] + "§7 um diesen zu bearbeiten.");
              all.sendMessage(msg22);
              all.sendMessage("§3§m§l---------------------------------");
            }
          }
        }
        else if (args[1].equalsIgnoreCase("Teaming"))
        {
          reporter.put(ProxyServer.getInstance().getPlayer(args[0]), p);
          reportee.add(p);
          p.sendMessage(Prefix + "§7Vielen Dank für deinen §eReport§7.");
          reported.put(ProxyServer.getInstance().getPlayer(args[0]), args[1]);
          for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
            if (all.hasPermission("bungeecord.command.kick") || all.hasPermission("bungeecord.*"))
            {
              TextComponent msg23 = new TextComponent(Prefix + "§7Klicke §e§lHIER§7 um den Report zu bearbeiten!");
              ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
              
              msg23.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cVerbinden").create()));
              msg23.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/r " + args[0]));
              all.sendMessage("§3§m§l---------------------------------");
              all.sendMessage(Prefix + "§7Der Spieler §e" + p.getName() + "§7 hat einen §eReport §7erstellt.");
              all.sendMessage(Prefix + "§7Gemeldeter Spieler: §e" + args[0]);
              all.sendMessage(Prefix + "§7Angegebener Grund: §e" + args[1]);
              all.sendMessage(Prefix + "§7Server: §e" + ProxyServer.getInstance().getPlayer(args[0]).getServer().getInfo().getName());
              all.sendMessage(Prefix + "§7Ping: §e" + target.getPing());
              all.sendMessage(Prefix + "§7Nutze §e/r " + args[0] + "§7 um diesen zu bearbeiten.");
              all.sendMessage(msg23);
              all.sendMessage("§3§m§l---------------------------------");
            }
          }
        }
        else if (args[1].equalsIgnoreCase("Werbung"))
        {
          reporter.put(ProxyServer.getInstance().getPlayer(args[0]), p);
          reportee.add(p);
          p.sendMessage(Prefix + "§7Vielen Dank für deinen §eReport§7.");
          reported.put(ProxyServer.getInstance().getPlayer(args[0]), args[1]);
          for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
            if (all.hasPermission("system.kick") || all.hasPermission("bungeecord.*"))
            {
              TextComponent msg24 = new TextComponent(Prefix + "§7Klicke §e§lHIER§7 um den Report zu bearbeiten!");
              msg24.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cVerbinden").create()));
              ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
              msg24.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/r " + args[0]));
              all.sendMessage("§3§m§l---------------------------------");
              all.sendMessage(Prefix + "§7Der Spieler §e" + p.getName() + "§7 hat einen §eReport §7erstellt.");
              all.sendMessage(Prefix + "§7Gemeldeter Spieler: §e" + args[0]);
              all.sendMessage(Prefix + "§7Angegebener Grund: §e" + args[1]);
              all.sendMessage(Prefix + "§7Server: §e" + ProxyServer.getInstance().getPlayer(args[0]).getServer().getInfo().getName());
              all.sendMessage(Prefix + "§7Ping: §e" + target.getPing());
              all.sendMessage(Prefix + "§7Nutze §e/r " + args[0] + "§7 um diesen zu bearbeiten.");
              all.sendMessage(msg24);
              all.sendMessage("§3§m§l---------------------------------");
            }
          }
        }
        else if (args[1].equalsIgnoreCase("Sonstiges"))
        {
          reporter.put(ProxyServer.getInstance().getPlayer(args[0]), p);
          reportee.add(p);
          p.sendMessage(Prefix + "§7Vielen Dank für deinen §eReport§7.");
          reported.put(ProxyServer.getInstance().getPlayer(args[0]), args[1]);
          for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
            if (all.hasPermission("bungeecord.command.kick") || all.hasPermission("bungeecord.*"))
            {
              TextComponent msg25 = new TextComponent(Prefix + "§7Klicke §e§lHIER§7 um den Report zu bearbeiten!");
              msg25.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cVerbinden").create()));
              ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
              msg25.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/r " + args[0]));
              all.sendMessage("§3§m§l---------------------------------");
              all.sendMessage(Prefix + "§7Der Spieler §e" + p.getName() + "§7 hat einen §eReport §7erstellt.");
              all.sendMessage(Prefix + "§7Gemeldeter Spieler: §e" + args[0]);
              all.sendMessage(Prefix + "§7Angegebener Grund: §e" + args[1]);
              all.sendMessage(Prefix + "§7Server: §e" + ProxyServer.getInstance().getPlayer(args[0]).getServer().getInfo().getName());
              all.sendMessage(Prefix + "§7Ping: §e" + target.getPing());
              all.sendMessage(Prefix + "§7Nutze §e/r " + args[0] + "§7 um diesen zu bearbeiten.");
              all.sendMessage(msg25);
              all.sendMessage("§3§m§l---------------------------------");
            }
          }
        }
        else
        {
          sender.sendMessage(Prefix + "§7Verwende /report <Name> <Hacks, Beleidigungen, Teaming, Werbung, Sonstiges>");
        }
      }
      else {
        p.sendMessage(Prefix + "§7Bitte warte, bis du einen weiteren §eReport §7erstellen kannst!");
      }
    }
  }
}
