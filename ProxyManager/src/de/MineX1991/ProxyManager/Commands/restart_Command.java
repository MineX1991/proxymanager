package de.MineX1991.ProxyManager.Commands;

import de.MineX1991.ProxyManager.Main.Data;
import de.MineX1991.ProxyManager.Main.Main;
import java.util.concurrent.TimeUnit;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.scheduler.TaskScheduler;

public class restart_Command
  extends Command
{
  public restart_Command(String string)
  {
    super(string);
  }
  
  public void execute(CommandSender arg0, String[] arg1)
  {
    ProxiedPlayer p = (ProxiedPlayer)arg0;
    if (p.hasPermission("bungeecord.command.restart") || p.hasPermission("bungeecord.*"))
    {
      BungeeCord.getInstance().broadcast(String.valueOf
        (Data.prefix) + "§cTryRusher.de §7startet in §ceiner Minute §7neu.");
      ProxyServer.getInstance().getScheduler().schedule(Main.plugin, new Runnable()
      {
        public void run()
        {
          BungeeCord.getInstance().broadcast(String.valueOf(Data.prefix) + "§cTryRusher.de §7startet in §c30 Sekunden §7neu.");
        }
      }, 30L, TimeUnit.SECONDS);
      ProxyServer.getInstance().getScheduler().schedule(Main.plugin, new Runnable()
      {
        public void run()
        {
          BungeeCord.getInstance().broadcast(String.valueOf(Data.prefix) + "§cTryRusher.de §7startet in §c10 Sekunden §7neu.");
        }
      }, 50L, TimeUnit.SECONDS);
      ProxyServer.getInstance().getScheduler().schedule(Main.plugin, new Runnable()
      {
        public void run()
        {
          BungeeCord.getInstance().broadcast(String.valueOf(Data.prefix) + "§cTryRusher.de §7startet in §c5 Sekunden §7neu.");
        }
      }, 55L, TimeUnit.SECONDS);
      ProxyServer.getInstance().getScheduler().schedule(Main.plugin, new Runnable()
      {
        public void run()
        {
          for (ProxiedPlayer p : ProxyServer.getInstance().getPlayers()) {
            p.disconnect(String.valueOf(Data.prefix) + "§7Der Server restartet.");
          }
        }
      }, 60L, TimeUnit.SECONDS);
      ProxyServer.getInstance().getScheduler().schedule(Main.plugin, new Runnable()
      {
        public void run()
        {
          ProxyServer.getInstance().stop();
        }
      }, 61L, TimeUnit.SECONDS);
    }
  }
}
