package de.MineX1991.ProxyManager.Commands;

import de.MineX1991.ProxyManager.Main.Data;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class Kick
  extends Command
{
  public Kick()
  {
    super("kick");
  }
  
  public void execute(CommandSender sender, String[] args)
  {
    ProxiedPlayer p = (ProxiedPlayer)sender;
    if (sender.hasPermission("bungeecord.command.kick") || sender.hasPermission("bungeecord.*"))
    {
      if (args.length < 2)
      {
        sender.sendMessage(String.valueOf(Data.prefix) + "§7Verwende §c/kick <Spieler> <Grund>");
      }
      else
      {
        if (ProxyServer.getInstance().getPlayer(args[0]) == null)
        {
          sender.sendMessage(String.valueOf(Data.prefix) + "§7Dieser §cSpieler §7ist nicht Online.");
          return;
        }
        String message = "";
        int i = 1;
        while (i < args.length)
        {
          message = String.valueOf(message) + args[i] + " ";
          i++;
        }
        sender.sendMessage("§7Du hast den §cSpieler §7erfolgreich vom Netzwerk geworfen.");
        ProxiedPlayer p2 = ProxyServer.getInstance().getPlayer(args[0]);
        for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
          if (all.hasPermission("bungeecord.command.kick") || sender.hasPermission("bungeecord.*"))
          {
            all.sendMessage(String.valueOf(Data.prefix) + "§c" + p2.getName() + "§7 wurde von §c" + p.getName() + "§7 von §3ClayMC.net §7gekickt.");
            all.sendMessage(String.valueOf(Data.prefix) + "§7Grund: §a" + message);
          }
        }
        String reason = message;
        p2.disconnect("§7Du wurdest vom §3TryRusher.de Netzwerk §7gekickt. \n §7Grund: §a" + reason);
      }
    }
    else {
      sender.sendMessage(String.valueOf(Data.prefix) + "§cDir fehlen die Rechte...");
    }
  }
}
