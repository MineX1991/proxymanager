package de.MineX1991.ProxyManager.Commands;

import de.MineX1991.ProxyManager.Main.Data;
import java.util.HashMap;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.plugin.Command;

public class JoinMe
  extends Command
{
  public static HashMap<ProxiedPlayer, Integer> used = new HashMap();
  
  public JoinMe(String name)
  {
    super(name);
  }
  
  public void execute(CommandSender sender, String[] args)
  {
    if (args.length == 0)
    {
      ProxiedPlayer p = (ProxiedPlayer)sender;
      if (p.hasPermission("bungeecord.command.joinme") || p.hasPermission("bungeecord.*")) {
        if (!p.hasPermission("bungeecord.joinme.use.nodelay"))
        {
          if (!used.containsKey(p)) {
            used.put(p, Integer.valueOf(0));
          }
          if (((Integer)used.get(p)).intValue() < 6) {
            for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers())
            {
              TextComponent msg2 = new TextComponent(String.valueOf("§8» §7KLICKE HIER, um den §eServer §7zu §ebetreten§7!"));
              msg2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cVerbinden").create()));
              msg2.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/xyserverxyx " + p.getServer().getInfo().getName()));
              all.sendMessage("§6§k---------------------------------");
              all.sendMessage(" ");
              if (p.getServer().getInfo().getName().startsWith("ClayJump")) {
                all.sendMessage(String.valueOf(Data.prefix) + "§6" + p.getName() + "§7 spielt §eClayJump§7!");
              } else if (p.getServer().getInfo().getName().startsWith("KnockbackFFA")) {
                all.sendMessage(String.valueOf(Data.prefix) + "§6" + p.getName() + "§7 spielt §eKnockbackFFA§7!");
              }
              if ((p.getServer().getInfo().getName().startsWith("KitBattle")) || (p.getServer().getInfo().getName().startsWith("NoHitDelay"))) {
                all.sendMessage(String.valueOf(Data.prefix) + "§6" + p.getName() + "§7 spielt §eKitBattle§7!");
              }
              if (p.getServer().getInfo().getName().equals("1vs1")) {
                all.sendMessage(String.valueOf(Data.prefix) + "§6" + p.getName() + "§7 spielt §e1vs1§7!");
              }
              if (p.getServer().getInfo().getName().startsWith("FreeBuild")) {
                all.sendMessage(String.valueOf(Data.prefix) + "§6" + p.getName() + "§7 spielt §eFreeBuild§7!");
              }
              if (p.getServer().getInfo().getName().equals("Bauevent")) {
                all.sendMessage(String.valueOf(Data.prefix) + "§6" + p.getName() + "§7 spielt §eBauevent§7!");
              }
              if (p.getServer().getInfo().getName().startsWith("UHC")) {
                all.sendMessage(String.valueOf(Data.prefix) + "§6" + p.getName() + "§7 spielt §eUHC§7!");
              }
              all.sendMessage(" ");
              all.sendMessage(msg2);
              all.sendMessage(" ");
              all.sendMessage("§6§k---------------------------------");
            }
          } else {
            p.sendMessage(String.valueOf(Data.prefix) + "§cDu hast diesen Befehl schon zu oft benutzt.");
          }
        }
        else
        {
          for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers())
          {
            TextComponent msg2 = new TextComponent(String.valueOf("§8» §7KLICKE HIER, um den §eServer §7zu §ebetreten§7!"));
            msg2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cVerbinden").create()));
            msg2.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/xyserverxyx " + p.getServer().getInfo().getName()));
            all.sendMessage("§6§k---------------------------------");
            all.sendMessage(" ");
            if (p.getServer().getInfo().getName().startsWith("ClayJump")) {
              all.sendMessage(String.valueOf(Data.prefix) + "§6" + p.getName() + "§7 spielt §eClayJump§7!");
            } else if (p.getServer().getInfo().getName().startsWith("KnockbackFFA")) {
              all.sendMessage(String.valueOf(Data.prefix) + "§6" + p.getName() + "§7 spielt §eKnockbackFFA§7!");
            }
            if ((p.getServer().getInfo().getName().startsWith("KitBattle")) || (p.getServer().getInfo().getName().startsWith("NoHitDelay"))) {
              all.sendMessage(String.valueOf(Data.prefix) + "§6" + p.getName() + "§7 spielt §eKitBattle§7!");
            }
            if (p.getServer().getInfo().getName().startsWith("1vs1")) {
              all.sendMessage(String.valueOf(Data.prefix) + "§6" + p.getName() + "§7 spielt §e1vs1§7!");
            }
            if (p.getServer().getInfo().getName().startsWith("FreeBuild")) {
              all.sendMessage(String.valueOf(Data.prefix) + "§6" + p.getName() + "§7 spielt §eFreeBuild§7!");
            }
            if (p.getServer().getInfo().getName().startsWith("Bauevent")) {
              all.sendMessage(String.valueOf(Data.prefix) + "§6" + p.getName() + "§7 spielt §eBauevent§7!");
            }
            if (p.getServer().getInfo().getName().startsWith("UHC")) {
              all.sendMessage(String.valueOf(Data.prefix) + "§6" + p.getName() + "§7 spielt §eUHC§7!");
            }
            all.sendMessage(" ");
            all.sendMessage(msg2);
            all.sendMessage(" ");
            all.sendMessage("§6§k---------------------------------");
          }
        }
      }
    }
    else if ((args.length == 1) && (args[0].equalsIgnoreCase("clear")) && (sender.hasPermission("bungeecord.command.joinme.clear")))
    {
      used.clear();
      sender.sendMessage("§cDu hast alle JoinMe Einträge geleert!");
    }
    else
    {
      sender.sendMessage("§cNein");
    }
  }
}
