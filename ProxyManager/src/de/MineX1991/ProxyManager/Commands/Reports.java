package de.MineX1991.ProxyManager.Commands;

import de.MineX1991.ProxyManager.Main.Data;
import java.util.ArrayList;
import java.util.HashMap;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.plugin.Command;

public class Reports
  extends Command
{
  public Reports()
  {
    super("r");
  }
  
  public void execute(CommandSender sender, String[] args)
  {
    ProxiedPlayer p = (ProxiedPlayer)sender;
    if (p.hasPermission("bungeecord.command.kick") || p.hasPermission("bungeecord.*")) {
      if (args.length == 1)
      {
        ProxiedPlayer p2 = ProxyServer.getInstance().getPlayer(args[0]);
        if (p2 != null)
        {
          if (Report.reported.containsKey(p2))
          {
            p.connect(p2.getServer().getInfo());
            p.sendMessage(Report.Prefix + "§7Verwende §e'/v'§7, um dich §eVanish §7zu machen!");
            Report.reported.remove(p2);
            ProxiedPlayer pp = (ProxiedPlayer)Report.reporter.get(p2);
            pp.sendMessage(Report.Prefix + "§7Dein §eReport §7über §e" + args[0] + "§7 wird nun von §e" + p.getName() + "§7 bearbeitet!");
            Report.reportee.remove(pp);
            Report.reporter.remove(pp);
            Report.reporter.remove(p2);
          }
          else
          {
            p.sendMessage(Report.Prefix + "§cDieser §eReport §cist nicht mehr §eöffentlich§c!");
          }
        }
        else {
          p.sendMessage(String.valueOf(Data.prefix) + "§cDer §eAngegebene Spieler §cist nicht mehr §eonline§c!");
        }
      }
      else
      {
        p.sendMessage(String.valueOf(Data.prefix) + "§cNutze §e/r [Spieler]");
      }
    }
  }
}
