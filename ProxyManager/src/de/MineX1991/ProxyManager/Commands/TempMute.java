package de.MineX1991.ProxyManager.Commands;

import de.MineX1991.ProxyManager.Main.Data;
import de.MineX1991.ProxyManager.Manager.MuteManager;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class TempMute
  extends Command
{
  public TempMute()
  {
    super("tempmute");
  }
  
  public void execute(CommandSender sender, String[] args)
  {
    if (sender.hasPermission("bungeecord.command.tempmute") || sender.hasPermission("bungeecord.*"))
    {
      if (args.length < 4)
      {
        sender.sendMessage(String.valueOf(Data.prefix) + "§cAusführung: /tempmute <Spieler> <Zeit> <Zeitform> <Grund> ");
      }
      else
      {
        if (MuteManager.isMuted(args[0]))
        {
          sender.sendMessage(String.valueOf(Data.prefix) + "§cDieser §cSpieler §cist bereits gemuted.");
          return;
        }
        String TimeUnit2 = args[2];
        String message = "";
        int i = 3;
        while (i < args.length)
        {
          message = String.valueOf(message) + args[i] + " ";
          i++;
        }
        int Time = Integer.parseInt(args[1]);
        if ((TimeUnit2.equalsIgnoreCase("sec")) || (TimeUnit2.equalsIgnoreCase("s")) || (TimeUnit2.equalsIgnoreCase("second")) || (TimeUnit2.equalsIgnoreCase("seconds")) || (TimeUnit2.equalsIgnoreCase("secs")))
        {
          sender.sendMessage(String.valueOf(Data.prefix) + "§7Du hast den Spieler §c" + args[0] + "§7 erfolgreich gemuted.");
          MuteManager.Mute(args[0], message, sender.getName(), Time * 1);
        }
        else if ((TimeUnit2.equalsIgnoreCase("min")) || (TimeUnit2.equalsIgnoreCase("minute")) || (TimeUnit2.equalsIgnoreCase("m")) || (TimeUnit2.equalsIgnoreCase("mins")) || (TimeUnit2.equalsIgnoreCase("minutes")))
        {
          sender.sendMessage(String.valueOf(Data.prefix) + "§7Du hast den Spieler §c" + args[0] + "§7 erfolgreich gemuted.");
          MuteManager.Mute(args[0], message, sender.getName(), Time * 60);
        }
        else if ((TimeUnit2.equalsIgnoreCase("h")) || (TimeUnit2.equalsIgnoreCase("hour")) || (TimeUnit2.equalsIgnoreCase("hours")))
        {
          sender.sendMessage(String.valueOf(Data.prefix) + "§7Du hast den Spieler §c" + args[0] + "§7 erfolgreich gemuted.");
          MuteManager.Mute(args[0], message, sender.getName(), Time * 60 * 60);
        }
        else if ((TimeUnit2.equalsIgnoreCase("d")) || (TimeUnit2.equalsIgnoreCase("day")) || (TimeUnit2.equalsIgnoreCase("days")))
        {
          sender.sendMessage(String.valueOf(Data.prefix) + "§7Du hast den Spieler §c" + args[0] + "§7 erfolgreich gemuted.");
          MuteManager.Mute(args[0], message, sender.getName(), Time * 60 * 60 * 24);
        }
        else if ((TimeUnit2.equalsIgnoreCase("w")) || (TimeUnit2.equalsIgnoreCase("week")) || (TimeUnit2.equalsIgnoreCase("weeks")))
        {
          sender.sendMessage(String.valueOf(Data.prefix) + "§7Du hast den Spieler §c" + args[0] + "§7 erfolgreich gemuted.");
          MuteManager.Mute(args[0], message, sender.getName(), Time * 60 * 60 * 24 * 7);
        }
        else
        {
          sender.sendMessage(String.valueOf(Data.prefix) + "§cVerwende folgende Zeitformen");
          sender.sendMessage(String.valueOf(Data.prefix) + "§c< s | m | h | d | w >");
        }
      }
    }
    else {
      sender.sendMessage(String.valueOf(Data.prefix) + "§7Keine Rechte.");
    }
  }
}
