package de.MineX1991.ProxyManager.Commands;

import de.MineX1991.ProxyManager.Main.Data;
import de.MineX1991.ProxyManager.Manager.MuteManager;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class UnMute
  extends Command
{
  public UnMute()
  {
    super("unmute");
  }
  
  public void execute(CommandSender sender, String[] args)
  {
    if (sender.hasPermission("bungeecord.command.unmute") || sender.hasPermission("bungeecord.*"))
    {
      if (args.length < 1)
      {
        sender.sendMessage(String.valueOf(Data.prefix) + "§7Ausführung:§c /unmute <Spieler>");
      }
      else
      {
        if (!MuteManager.isMuted(args[0]))
        {
          sender.sendMessage(String.valueOf(Data.prefix) + "§7Dieser §cSpieler§7 ist nicht gemuted.");
          return;
        }
        sender.sendMessage(String.valueOf(Data.prefix) + "§7Dieser §cSpieler §7wurde erfolgreich entmuted.");
        MuteManager.unMute(args[0], sender.getName());
      }
    }
    else {
      sender.sendMessage(String.valueOf(Data.prefix) + "§cKeine Rechte.");
    }
  }
}
