package de.MineX1991.ProxyManager.Commands;

import de.MineX1991.ProxyManager.Main.Data;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class broadcast
  extends Command
{
  public broadcast()
  {
    super("bc");
  }
  
  public void execute(CommandSender sender, String[] args)
  {
    if (sender.hasPermission("bungeecord.command.bc") || sender.hasPermission("bungeecord.*"))
    {
      if (args.length == 0)
      {
        sender.sendMessage(String.valueOf(Data.prefix) + "§7Verwende §c/bc <Nachricht>");
      }
      else if (args.length >= 1)
      {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (i < args.length)
        {
          sb.append(String.valueOf(args[i]) + " ");
          i++;
        }
        String st = sb.toString();
        for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers())
        {
          all.sendMessage("");
          all.sendMessage("§cTryAlert §8§l•» §7" + st.replace("&", "§"));
          all.sendMessage("");
        }
      }
    }
    else {
      sender.sendMessage(String.valueOf(Data.prefix) + "§cDir fehlen die Rechte an diesem Befehl!");
    }
  }
}
