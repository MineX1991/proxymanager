package de.MineX1991.ProxyManager.Commands;

import de.MineX1991.ProxyManager.Main.Data;
import de.MineX1991.ProxyManager.Manager.BanManager;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class UnBan
  extends Command
{
  public UnBan()
  {
    super("unban");
  }
  
  public void execute(CommandSender sender, String[] args)
  {
    if (sender.hasPermission("bungeecord.command.unban") || sender.hasPermission("bungeecord.*"))
    {
      if (args.length != 1)
      {
        sender.sendMessage(String.valueOf(Data.prefix) + "§7Ausführung:§c /unban <Spieler>");
      }
      else
      {
        if (!BanManager.isBanned(args[0]))
        {
          sender.sendMessage(String.valueOf(Data.prefix) + "§7Dieser §cSpieler §7ist nicht gebannt.");
          return;
        }
        sender.sendMessage(String.valueOf(Data.prefix) + "§7Der §cSpieler §7wurde entbannt.");
        BanManager.unBan(args[0], sender.getName());
      }
    }
    else {
      sender.sendMessage(String.valueOf(Data.prefix) + "§cKeine Rechte.");
    }
  }
}
