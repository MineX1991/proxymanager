package de.MineX1991.ProxyManager.Commands;

import de.MineX1991.ProxyManager.Main.Data;
import java.util.HashMap;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.plugin.Command;

public class ClearChat
  extends Command
{
  public static HashMap<Server, String> muted = new HashMap();
  
  public ClearChat()
  {
    super("cc");
  }
  
  public void execute(CommandSender sender, String[] args)
  {
    ProxiedPlayer p = (ProxiedPlayer)sender;
    if (sender.hasPermission("bungeecord.command.clearchat") || sender.hasPermission("bungeecord.*"))
    {
      if (args.length != 0)
      {
        sender.sendMessage(String.valueOf(Data.prefix) + "§7Verwende §c/cc");
      }
      else
      {
        int i = 0;
        while (i < 200)
        {
          for (ProxiedPlayer all : p.getServer().getInfo().getPlayers()) {
            all.sendMessage(" ");
          }
          i++;
        }
        for (ProxiedPlayer all : p.getServer().getInfo().getPlayers()) {
          all.sendMessage(String.valueOf(Data.prefix) + "§7Der §aChat §7wurde geleert!");
        }
      }
    }
    else {
      sender.sendMessage(String.valueOf(Data.prefix) + "§cKeine Rechte.");
    }
  }
}
