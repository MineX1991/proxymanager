package de.MineX1991.ProxyManager.Commands;

import de.MineX1991.ProxyManager.Manager.MuteManager;
import java.util.List;
import net.md_5.bungee.api.CommandSender;
import de.MineX1991.ProxyManager.Main.Data;
import net.md_5.bungee.api.plugin.Command;

public class MuteList
  extends Command
{
  public MuteList()
  {
    super("mutelist");
  }
  
  public void execute(CommandSender sender, String[] args)
  {
    if (sender.hasPermission("bungeecord.command.mutelist") || sender.hasPermission("bungeecord.*"))
    {
      if (MuteManager.getMutedPlayers().size() == 0)
      {
        sender.sendMessage(String.valueOf(Data.prefix) + "§7Es sind derzeit keine §cSpieler §7gemuted.");
        return;
      }
      sender.sendMessage(String.valueOf(Data.prefix) + "§7Diese §cSpieler §7sind derzeit Gemuted:");
      for (String x : MuteManager.getMutedPlayers()) {
        sender.sendMessage("§c" + x + " §8| §7/check " + x);
      }
    }
    else
    {
      sender.sendMessage(String.valueOf(Data.prefix) + "§cKeine Rechte!");
    }
  }
}
