package de.MineX1991.ProxyManager.Commands;

import de.MineX1991.ProxyManager.Main.Data;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class Mute
  extends Command
{
  public Mute()
  {
    super("mute");
  }
  
  public void execute(CommandSender sender, String[] args)
  {
    if (sender.hasPermission("bungeecord.command.mute") || sender.hasPermission("bungeecord.*")) {
      sender.sendMessage(String.valueOf(Data.prefix) + "§7Nutze §c/strafe <Name> <Grund>");
    } else {
      sender.sendMessage(String.valueOf(Data.prefix) + "§cUnbekannter Command.");
    }
  }
}
