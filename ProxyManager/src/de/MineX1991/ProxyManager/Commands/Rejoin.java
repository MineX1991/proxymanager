package de.MineX1991.ProxyManager.Commands;

import de.MineX1991.ProxyManager.Lobby.Hu;
import de.MineX1991.ProxyManager.Main.Data;
import de.MineX1991.ProxyManager.Main.Main;
import java.util.concurrent.TimeUnit;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * @author MineX1991
 */
public class Rejoin extends Command {

    public Rejoin() {
        super("rc");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer pp = (ProxiedPlayer) sender;
        if (pp.hasPermission("bungeecord.command.rejoin") || pp.hasPermission("bungeecord.*")) {
            if (!pp.getServer().getInfo().getName().equalsIgnoreCase("lobby")) {
                pp.sendMessage(new TextComponent("§l"));
                pp.sendMessage(new TextComponent(Data.prefix + "§3Bitte bleibe einen Moment stehen!"));
                pp.sendMessage(new TextComponent(Data.prefix + "§eDer server wird nun den rest erledigen!"));
                pp.sendMessage(new TextComponent("§l "));
                ServerInfo test = ProxyServer.getInstance().getServerInfo(pp.getServer().getInfo().getName());
                BungeeCord.getInstance().getScheduler().schedule(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        Hu.send(pp);
                    }
                }, 2, TimeUnit.SECONDS);
                BungeeCord.getInstance().getScheduler().schedule(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        pp.connect(test);
                    }
                }, 4, TimeUnit.SECONDS);
            } else {
                pp.sendMessage(new TextComponent(Data.prefix + "§c... NOT HERE ..."));
            }
        } else {
            pp.sendMessage(new TextComponent(Data.prefix + "§4Keine Rechte!"));
        }
    }    
}