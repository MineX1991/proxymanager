package de.MineX1991.ProxyManager.Commands;

import java.util.Collection;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.plugin.Command;

public class List
  extends Command
{
  public List()
  {
    super("list");
  }
  
  public void execute(CommandSender sender, String[] args)
  {
    ProxiedPlayer p = (ProxiedPlayer)sender;
    p.sendMessage("§7§l§m------------  §e§lTryRusher.de §8§l- §7§lList §7§l§m------------");
    p.sendMessage(" ");
    p.sendMessage("§7Spieler auf dem Netzwerk: §a" + BungeeCord.getInstance().getOnlineCount());
    p.sendMessage("§7Spieler auf §c" + p.getServer().getInfo().getName() + "§7:§a " + p.getServer().getInfo().getPlayers().size());
    p.sendMessage(" ");
    p.sendMessage("§7§l§m------------  §e§lTryRusher.de §8§l- §7§lList §7§l§m------------");
  }
}
