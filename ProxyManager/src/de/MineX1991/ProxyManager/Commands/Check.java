package de.MineX1991.ProxyManager.Commands;

import de.MineX1991.ProxyManager.Main.Data;
import de.MineX1991.ProxyManager.Manager.BanManager;
import de.MineX1991.ProxyManager.Manager.MuteManager;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class Check
  extends Command
{
  public Check()
  {
    super("check");
  }
  
  public void execute(CommandSender sender, String[] args)
  {
    if (sender.hasPermission("bungeecord.command.check") || sender.hasPermission("bungeecord.*"))
    {
      if (args.length == 0)
      {
        sender.sendMessage(String.valueOf(Data.prefix) + "§7Ausführung: §c/check <Spieler>");
      }
      else
      {
        if (BanManager.isBanned(args[0]))
        {
          sender.sendMessage("§3§m---------------------------------");
          sender.sendMessage("§eSpieler: §a" + args[0]);
          sender.sendMessage("§eGebannt von: §3" + BanManager.getWhoBanned(args[0]));
          sender.sendMessage("§eGrund: §3" + BanManager.getReason(args[0]));
          sender.sendMessage("§eVerbleibende Zeit: §3" + BanManager.getRemainingTime(args[0]));
        }
        else
        {
          sender.sendMessage("");
          sender.sendMessage("§cDer Spieler ist nicht gebannt!");
          sender.sendMessage("§3§m---------------------------------");
          sender.sendMessage("");
        }
        if (MuteManager.isMuted(args[0]))
        {
          sender.sendMessage("§eSpieler: §a" + args[0]);
          sender.sendMessage("§eGemuted von: §3" + MuteManager.getWhoMuted(args[0]));
          sender.sendMessage("§eGrund: §3" + MuteManager.getReason(args[0]));
          sender.sendMessage("§eVerbleibende Zeit: §3" + MuteManager.getRemainingTime(args[0]));
          sender.sendMessage("§3§m---------------------------------");
        }
        else
        {
          sender.sendMessage("§cDer Spieler ist nicht gemuted!");
        }
      }
    }
    else {
      sender.sendMessage(String.valueOf(Data.prefix) + "§cDu hast keine Rechte...");
    }
  }
}
