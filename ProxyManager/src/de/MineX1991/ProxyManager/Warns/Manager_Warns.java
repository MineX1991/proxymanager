package de.MineX1991.ProxyManager.Warns;

import de.MineX1991.ProxyManager.MOTD.Config;
import java.util.ArrayList;

public class Manager_Warns
{
  public static ArrayList<String> listWarns()
  {
    return Config.Config.PlayerWarns;
  }
  
  public static Integer getWarns(String p)
  {
    for (String warn : Config.Config.PlayerWarns) {
      if (warn.split(" ")[0].equalsIgnoreCase(p)) {
        return Integer.valueOf(warn.split(" ")[1]);
      }
    }
    return Integer.valueOf(0);
  }
  
  public static void addWarn(String p)
  {
    for (String warn : Config.Config.PlayerWarns) {
      if (warn.split(" ")[0].equalsIgnoreCase(p))
      {
        Config.Config.PlayerWarns.remove(warn);
        Config.Config.PlayerWarns.add(String.valueOf(p) + " " + (Integer.valueOf(warn.split(" ")[1]).intValue() + 1));
        Config.SaveConfig();
        return;
      }
    }
    Config.Config.PlayerWarns.add(String.valueOf(p) + " 1");
    Config.SaveConfig();
  }
  
  public static void setWarns(String p, Integer Warns)
  {
    for (String warn : Config.Config.PlayerWarns) {
      if (warn.split(" ")[0].equalsIgnoreCase(p))
      {
        Config.Config.PlayerWarns.remove(warn);
        if (Warns.intValue() != 0) {
          Config.Config.PlayerWarns.add(String.valueOf(p) + " " + Warns);
        }
        Config.SaveConfig();
        return;
      }
    }
    if (Warns.intValue() != 0)
    {
      Config.Config.PlayerWarns.add(String.valueOf(p) + " " + Warns);
      Config.SaveConfig();
    }
  }
}
