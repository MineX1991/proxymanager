package de.MineX1991.ProxyManager.Warns;

import de.MineX1991.ProxyManager.MOTD.Config;
import java.util.ArrayList;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class Command_WerbungList
  extends Command
{
  public Command_WerbungList(String string)
  {
    super(string);
  }
  
  public void execute(CommandSender sender, String[] args)
  {
    if (sender.hasPermission("bungeecord.werbungmute"))
    {
      if (args.length == 1)
      {
        if ((args[0].equalsIgnoreCase("reset")) || (args[0].equalsIgnoreCase("clear")))
        {
          sender.sendMessage(String.valueOf(Config.Config.Werbungsserver.size()) + " Server wurden gefunden.");
          Config.Config.Werbungsserver.clear();
          Config.SaveConfig();
          return;
        }
        sender.sendMessage(ChatColor.RED + "/werbunglist [clear]");
      }
      else if (Config.Config.Werbungsserver.size() < 19)
      {
        sender.sendMessage(ChatColor.DARK_AQUA + "----------" + ChatColor.GOLD + "Es sind " + Config.Config.Werbungsserver.size() + " Server gefunden." + ChatColor.DARK_AQUA + "----------");
        String OnlinePlayers = "";
        for (String s : Config.Config.Werbungsserver) {
          OnlinePlayers = String.valueOf(OnlinePlayers) + "," + s.split(" ")[0];
        }
        sender.sendMessage(ChatColor.DARK_RED + OnlinePlayers);
      }
      else
      {
        sender.sendMessage(ChatColor.RED + "Die Server k�nnen nicht angezeigt werden. Grund:" + ChatColor.YELLOW + " Zu viele Server.");
      }
    }
    else {
      sender.sendMessage("�cDaf�r hast du keine Rechte.");
    }
  }
}
