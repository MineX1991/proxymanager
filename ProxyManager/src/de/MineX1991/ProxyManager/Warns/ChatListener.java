package de.MineX1991.ProxyManager.Warns;

import de.MineX1991.ProxyManager.MOTD.Config;
import de.MineX1991.ProxyManager.MOTD.Manager_Chat;
import de.MineX1991.ProxyManager.Main.Data;
import de.MineX1991.ProxyManager.Main.Main;
import java.util.ArrayList;
import java.util.List;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.event.ServerKickEvent;
import net.md_5.bungee.api.event.TabCompleteEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class ChatListener
  implements Listener
{
  public static ArrayList<ProxiedPlayer> cooldown = new ArrayList();
  
  private static Boolean isMuted(String PlayerName)
  {
    if (Config.Config.MutedPlayers.contains(PlayerName)) {
      return Boolean.valueOf(true);
    }
    return Boolean.valueOf(false);
  }
  
  private static Boolean isMuted1(String PlayerName)
  {
    if (Config.Config.MutedWerbung.contains(PlayerName)) {
      return Boolean.valueOf(true);
    }
    return Boolean.valueOf(false);
  }
  
  @EventHandler
  public void onChat(ChatEvent e)
  {
    List<String> ads = Main.plugin.getAds();
    List<String> plugins = Main.plugin.getPlugins();
    List<String> wort = Main.plugin.getWort();
    ProxiedPlayer pl = (ProxiedPlayer)e.getSender();
    if (((pl instanceof ProxiedPlayer)) && (!e.isCommand()) && (isMuted1(pl.getName()).booleanValue()))
    {
      e.setCancelled(true);
      pl.sendMessage(String.valueOf(Data.prefix) + "§7Der Chat wird dir untersagt. §cGrund: Du bist gemuted");
    }
    for (String p : plugins) {
      if ((e.getMessage().equals(p)) && (!pl.hasPermission("bungeecord.command.plugins")))
      {
        pl.sendMessage("§cNicht verfügbar!");
        e.setCancelled(true);
      }
    }
    for (String w : wort) {
      if ((e.getMessage().contains(w)) && (!pl.hasPermission("bungeecord.command.wort")))
      {
        pl.sendMessage(String.valueOf(Data.prefix) + "§7Diese Chatnachricht wurde vom System gefiltert. §cGrund: Anzügige Sprache");
        for (ProxiedPlayer p2 : BungeeCord.getInstance().getPlayers()) {
          if (p2.hasPermission("werbung.show")) {
            MessageListener.getInstance().sendMessage(p2, MessageType.INFO, new String[] { "§7Der Spieler §c" + pl.getDisplayName() + "§7 hat geschrieben: §c" + e.getMessage() + " §7auf §c" + pl.getServer().getInfo().getName() });
          }
        }
        e.setCancelled(true);
      }
    }
    if (!pl.hasPermission("werbung.enable")) {
      for (String a : ads) {
        if ((e.getMessage().contains(a)) && (!e.getMessage().toLowerCase().contains("TryRusher.de")))
        {
          for (ProxiedPlayer p2 : BungeeCord.getInstance().getPlayers()) {
            if (p2.hasPermission("werbung.show")) {
              MessageListener.getInstance().sendMessage(p2, MessageType.INFO, new String[] { "§7Der Spieler §c" + pl.getDisplayName() + "§7 hat geschrieben: §c" + e.getMessage() + " §7auf §c" + pl.getServer().getInfo().getName() });
            }
          }
          e.setCancelled(true);
          addWerbung(e.getMessage());
          Config.SaveConfig();
          MessageListener.getInstance().sendMessage(pl, MessageType.BAD, new String[] { "§7Deine ChatNachricht wurde gefiltert. §cGrund: Mögliche Werbung." });
        }
      }
    }
  }
  
  @EventHandler
  public void onJoin(ServerConnectEvent e)
  {
    ProxiedPlayer pl = e.getPlayer();
    if (((pl instanceof ProxiedPlayer)) && (Manager_Chat.Wartungen.booleanValue()))
    {
      if (!pl.hasPermission("bungeecord.wartungen.join"))
      {
        pl.disconnect(String.valueOf(Config.Config.Prefix) + "§7Du kannst §cTryRusher.de §7derzeit nicht betreten. \n §7Derzeit sind §cWartungsarbeiten§7.");
        e.setCancelled(true);
      }
      return;
    }
  }
  
  @EventHandler
  public void onKick(ServerKickEvent event)
  {
    if (event.getPlayer().getServer().getInfo().getName() != "lobby")
    {
      if (!event.getKickReason().startsWith(".0"))
      {
        event.setCancelled(true);
        event.getPlayer().sendMessage(String.valueOf(Data.prefix) + "§7Deine §cVerbindung §7zum Server §c" + event.getKickedFrom().getName() + "§7 wurde getrennt. Grund: §c" + event.getKickReason());
      }
    }
    else {
      event.getPlayer().sendMessage(String.valueOf(Data.prefix) + "§7Deine §cVerbindung §7zum Server §c" + event.getKickedFrom().getName() + "§7 wurde getrennt.");
    }
  }
  
  @EventHandler
  public void onTab(TabCompleteEvent e)
  {
    String Cursor = e.getCursor().toLowerCase();
    ProxiedPlayer p = (ProxiedPlayer)e.getSender();
    if ((!p.hasPermission("System.tab")) && (Cursor.startsWith("/"))) {
      e.setCancelled(true);
    }
  }
  
  private String ContainsMute(String PlayerName)
  {
    for (String s : Config.Config.MutedWerbung) {
      if (s.equalsIgnoreCase(PlayerName)) {
        return s;
      }
    }
    return null;
  }
  
  private void removeMute(String PlayerName)
  {
    ArrayList<String> removeList = new ArrayList();
    for (String s : Config.Config.MutedWerbung) {
      if (s.equalsIgnoreCase(PlayerName)) {
        removeList.add(s);
      }
    }
    for (String s2 : removeList) {
      Config.Config.MutedWerbung.remove(s2);
    }
  }
  
  private void addMute(String PlayerName)
  {
    removeMute(PlayerName);
    Config.Config.MutedWerbung.add(PlayerName);
  }
  
  private void addWerbung(String Werbung)
  {
    removeMute(Werbung);
    Config.Config.Werbungsserver.add(Werbung);
  }
  
  private String getRealName(String PlayerName)
  {
    for (ProxiedPlayer p : ProxyServer.getInstance().getPlayers()) {
      if (p.getName().equalsIgnoreCase(PlayerName)) {
        return p.getName();
      }
    }
    return null;
  }
  
  @EventHandler
  public void on(ServerKickEvent e)
  {
    ServerInfo s1 = ProxyServer.getInstance().getServerInfo("Lobby-01");
    e.getPlayer().connect(s1);
  }
}
