package de.MineX1991.ProxyManager.Warns;

import de.MineX1991.ProxyManager.MOTD.Config;
import de.MineX1991.ProxyManager.MOTD.Manager_Chat;
import java.util.ArrayList;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.plugin.Command;

public class Command_Warns
  extends Command
{
  public Command_Warns(String string)
  {
    super(string);
  }
  
  public void execute(CommandSender sender, String[] args)
  {
    if (sender.hasPermission("system.warns"))
    {
      if (args.length >= 1)
      {
        if ((args[0].equalsIgnoreCase("clear")) || (args[0].equalsIgnoreCase("reset")))
        {
          if (args.length == 2)
          {
            sender.sendMessage(Manager_Chat.getComponent(null, ChatColor.GREEN + "Hier klicken", ChatColor.RED + ", um das Zurücksetzen der Warns von", new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/warns clear* " + args[1]), ChatColor.GREEN + "Alle Warns zurücksetzen..."));
            sender.sendMessage(ChatColor.RED + "\"" + ChatColor.GOLD + args[1] + ChatColor.RED + "\" zu best�tigen.");
          }
          else if (args.length == 1)
          {
            sender.sendMessage(Manager_Chat.getComponent(null, ChatColor.GREEN + "Hier klicken", ChatColor.RED + ", um das Zurücksetzen aller Warns zu bestätigen.", new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/warns clear*"), ChatColor.GREEN + "Alle Warns zurücksetzen..."));
          }
          else
          {
            sender.sendMessage(ChatColor.RED + "/warns <clear|get|list> [player]");
          }
          return;
        }
        if (args[0].equalsIgnoreCase("clear*"))
        {
          if (args.length == 2)
          {
            Integer oldWarns = Manager_Warns.getWarns(args[1]);
            Manager_Warns.setWarns(args[1], Integer.valueOf(0));
            sender.sendMessage(ChatColor.GREEN + "Die " + ChatColor.YELLOW + "Warns (" + oldWarns + " Warns) " + ChatColor.GREEN + "von \"" + ChatColor.GOLD + args[1] + ChatColor.GREEN + "\", wurden erfolgreich resetet.");
          }
          if (args.length == 1)
          {
            Config.Config.PlayerWarns.clear();
            if (!Config.SaveConfig())
            {
              sender.sendMessage(ChatColor.RED + "Fehler: java.lang.file_write_error!");
              return;
            }
            sender.sendMessage(ChatColor.GREEN + "Alle Warns wurden erfolgreich resetet.");
          }
          return;
        }
        if (args[0].equalsIgnoreCase("get"))
        {
          if (args.length == 2) {
            sender.sendMessage(ChatColor.GREEN + "Verwarnungen von " + ChatColor.GOLD + args[1] + ChatColor.GREEN + ": " + ChatColor.YELLOW + Manager_Warns.getWarns(args[1]) + ChatColor.GREEN + ".");
          } else {
            sender.sendMessage(ChatColor.RED + "/warns <clear|get|list> [player]");
          }
          return;
        }
        if (args[0].equalsIgnoreCase("list"))
        {
          if (Manager_Warns.listWarns().size() < 19)
          {
            sender.sendMessage(ChatColor.DARK_AQUA + "----------" + ChatColor.GOLD + "Verwarnungen (" + Manager_Warns.listWarns().size() + ")" + ChatColor.DARK_AQUA + "----------");
            for (String s : Manager_Warns.listWarns()) {
              if (s.contains(" ")) {
                sender.sendMessage(ChatColor.YELLOW + "  " + s.split(" ")[0] + ": " + ChatColor.RED + s.split(" ")[1]);
              }
            }
          }
          else
          {
            sender.sendMessage(ChatColor.RED + "Die Spieler können nicht angezeigt werden. Grund:" + ChatColor.YELLOW + " Zu viele Spieler.");
          }
          return;
        }
        sender.sendMessage(ChatColor.RED + "/warns <clear|get|list> [player]");
      }
      else
      {
        sender.sendMessage(ChatColor.RED + "/warns <clear|get|list> [player]");
      }
    }
    else {
      sender.sendMessage("�cDaf�r hast du keine Rechte.");
    }
  }
}
