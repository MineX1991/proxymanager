package de.MineX1991.ProxyManager.Warns;

import de.MineX1991.ProxyManager.Main.Data;
import java.io.PrintStream;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class MessageListener
{
  private static MessageListener instance = new MessageListener();
  private String prefix = String.valueOf(Data.prefix) + ChatColor.RESET;
  
  public static MessageListener getInstance()
  {
    return instance;
  }
  
  public void sendMessage(ProxiedPlayer p, MessageType type, String[] messages)
  {
    String[] arrstring = messages;
    int n = arrstring.length;
    int n2 = 0;
    while (n2 < n)
    {
      String msg2 = arrstring[n2];
      p.sendMessage(new TextComponent(String.valueOf(this.prefix) + type.getColor() + msg2));
      n2++;
    }
  }
  
  public void sendMessage(MessageType type, String[] messages)
  {
    String[] arrstring = messages;
    int n = arrstring.length;
    int n2 = 0;
    while (n2 < n)
    {
      String msg2 = arrstring[n2];
      System.out.println(String.valueOf(this.prefix) + type.getColor() + msg2);
      n2++;
    }
  }
}
