package de.MineX1991.ProxyManager.Warns;

import de.MineX1991.ProxyManager.MOTD.Config;
import de.MineX1991.ProxyManager.MOTD.Manager_Chat;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class Command_Warn
  extends Command
{
  private String LastWarn = "- -";
  
  public Command_Warn(String string)
  {
    super("asdasdaaaaa");
  }
  
  public void execute(CommandSender sender, String[] args)
  {
    if (sender.hasPermission("bungeecord.command.warn") || sender.hasPermission("bungeecord.*"))
    {
      if (args.length == 0)
      {
        sender.sendMessage(ChatColor.RED + "§c/warn <player> [reason]");
      }
      else
      {
        ProxiedPlayer p = null;
        for (ProxiedPlayer pl : ProxyServer.getInstance().getPlayers()) {
          if (pl.getName().equalsIgnoreCase(args[0]))
          {
            if (pl.hasPermission("bungeecord.command.warn") || sender.hasPermission("bungeecord.*"))
            {
              sender.sendMessage(ChatColor.RED + "§7Du kannst diesen§c Spieler§7 nicht warnen.");
              return;
            }
            p = pl;
            break;
          }
        }
        if (((sender instanceof ProxiedPlayer)) && (sender.getName().equalsIgnoreCase(args[0])))
        {
          sender.sendMessage(ChatColor.RED + "§7Du kannst dich nicht selbst warnen!");
          return;
        }
        if (p == null)
        {
          if (args[0].endsWith("*"))
          {
            if (!this.LastWarn.equals(String.valueOf(sender.getName()) + " " + args[0].toLowerCase()))
            {
              askWarn(sender, args[0].replace("*", ""), null);
              this.LastWarn = (String.valueOf(sender.getName()) + " " + args[0].toLowerCase());
            }
            return;
          }
          this.LastWarn = "- -";
          sender.sendMessage(ChatColor.RED + "§7Der §cSpieler§7 wurde nicht gefunden oder ist offline! ");
          if ((sender instanceof ProxiedPlayer)) {
            sender.sendMessage(Manager_Chat.getComponent(null, ChatColor.GREEN + "Hier klicken", ChatColor.RED + ", um \"" + ChatColor.GOLD + args[0] + ChatColor.RED + "\" trotzdem zu warnen.", new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/warn " + args[0] + "*"), ChatColor.GREEN + args[0] + " trotzdem warnen..."));
          }
          return;
        }
        String Message = null;
        if (args.length > 1)
        {
          Message = "";
          Integer count = Integer.valueOf(1);
          while (count.intValue() < args.length)
          {
            Message = String.valueOf(Message) + " " + args[count.intValue()];
            count = Integer.valueOf(count.intValue() + 1);
          }
          if ((Message = Manager_Chat.getMessage(sender, Message)) == null) {
            return;
          }
        }
        askWarn(sender, p.getName(), Message);
      }
    }
    else {
      sender.sendMessage("�cDaf�r hast du keine Rechte.");
    }
  }
  
  public void askWarn(CommandSender sender, String PlayerName, String Reason)
  {
    if (Warn(sender, PlayerName, Reason).booleanValue())
    {
      for (String bp : Config.Config.BannedPlayers) {
        if ((bp.length() >= 3) && (PlayerName.equalsIgnoreCase(bp.split(" ")[0]))) {
          return;
        }
      }
      sender.sendMessage("");
      sender.sendMessage(ChatColor.RED + "Der Spieler " + ChatColor.GOLD + PlayerName + ChatColor.RED + " sollte jetzt, wegen");
      sender.sendMessage(ChatColor.RED + "zuvielen " + ChatColor.YELLOW + "Warns" + ChatColor.DARK_RED + " gebannt " + ChatColor.RED + "werden.");
      sender.sendMessage(Manager_Chat.getComponent(null, ChatColor.GREEN + "Hier klicken", ChatColor.RED + ", um das Bannen von \"" + ChatColor.GOLD + PlayerName + ChatColor.RED + "\" zu bestätigen.", new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/ban " + PlayerName + "**"), ChatColor.GREEN + PlayerName + " vom Server bannen..."));
    }
  }
  
  public Boolean Warn(CommandSender Who, String PlayerName, String Reason)
  {
    Manager_Warns.addWarn(PlayerName);
    Integer warns = Manager_Warns.getWarns(PlayerName);
    ProxiedPlayer p = ProxyServer.getInstance().getPlayer(PlayerName);
    if (Reason == null) {
      ProxyServer.getInstance().broadcast(ChatColor.DARK_AQUA + "Der Spieler " + ChatColor.RED + ChatColor.BOLD + "\"" + PlayerName + "\"" + ChatColor.DARK_AQUA + " bekam einen " + ChatColor.GOLD + "Warn " + ChatColor.DARK_AQUA + "(" + ChatColor.YELLOW + warns + ChatColor.DARK_AQUA + "/" + ChatColor.YELLOW + Config.Config.MaxWarns + ChatColor.DARK_AQUA + ").");
    } else {
      ProxyServer.getInstance().broadcast(ChatColor.DARK_AQUA + "Der Spieler " + ChatColor.RED + ChatColor.BOLD + "\"" + PlayerName + "\"" + ChatColor.DARK_AQUA + " bekam einen " + ChatColor.GOLD + "Warn " + ChatColor.DARK_AQUA + "(" + ChatColor.YELLOW + warns + ChatColor.DARK_AQUA + "/" + ChatColor.YELLOW + Config.Config.MaxWarns + ChatColor.DARK_AQUA + "). " + ChatColor.RED + "Grund: " + ChatColor.YELLOW + Reason);
    }
    if (p != null)
    {
      if (warns != null) {
        p.sendMessage(ChatColor.GOLD + "Du hast " + ChatColor.RED + warns + ChatColor.GOLD + "/" + ChatColor.GOLD + Config.Config.MaxWarns + ChatColor.RED + " Warns.");
      }
      if (warns.intValue() >= Config.Config.MaxWarns.intValue()) {
        p.disconnect(ChatColor.RED + "Du wurdest wegen " + ChatColor.GOLD + warns + ChatColor.RED + "/" + ChatColor.RED + Config.Config.MaxWarns + ChatColor.GOLD + " Warns vom Server geworfen.");
      }
    }
    if (warns.intValue() >= Config.Config.MaxWarns.intValue()) {
      return Boolean.valueOf(true);
    }
    return Boolean.valueOf(false);
  }
}
