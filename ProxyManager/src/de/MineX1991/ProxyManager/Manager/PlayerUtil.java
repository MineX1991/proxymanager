package de.MineX1991.ProxyManager.Manager;

import com.google.common.base.Charsets;
import java.util.UUID;

/**
 * @author MineX1991
 */
public class PlayerUtil {
    public static String getUUID(String playername) {
        return UUID.nameUUIDFromBytes(("OfflinePlayer:" + playername).getBytes(Charsets.UTF_8)).toString();
    }
}