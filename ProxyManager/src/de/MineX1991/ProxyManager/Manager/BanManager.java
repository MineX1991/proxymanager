package de.MineX1991.ProxyManager.Manager;

import de.MineX1991.ProxyManager.Main.Data;
import java.util.ArrayList;
import java.util.List;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.config.Configuration;

public class BanManager
{
  static Configuration cfg = Files.BanConfig;
  static Configuration cfgi = Files.BanConfig;
  
  public static boolean exists(String playername)
  {
    if (cfg.get("Spieler." + PlayerUtil.getUUID(playername)) != null) {
      return true;
    }
    return false;
  }
  
  public static void createPlayer(String Spielername)
  {
    if (!exists(Spielername))
    {
      cfg.set("Spieler." + PlayerUtil.getUUID(Spielername) + ".Spielername", Spielername);
      cfg.set("Spieler." + PlayerUtil.getUUID(Spielername) + ".Ban", Boolean.valueOf(false));
      cfg.set("Spieler." + PlayerUtil.getUUID(Spielername) + ".Grund", "");
      cfg.set("Spieler." + PlayerUtil.getUUID(Spielername) + ".von", "");
      cfg.set("Spieler." + PlayerUtil.getUUID(Spielername) + ".Ende", Integer.valueOf(0));
      Files.saveBanFile();
    }
  }
  
  public static boolean isBanned(String Spielername)
  {
    if (exists(Spielername)) {
      return cfg.getBoolean("Spieler." + PlayerUtil.getUUID(Spielername) + ".Ban");
    }
    return false;
  }
  
  public static void Ban(String Spielername, String Grund, String von, int Sekunden)
  {
    if (!isBanned(Spielername))
    {
      long current = System.currentTimeMillis();
      long end = current + Sekunden * 1000;
      if (Sekunden == -1) {
        end = -1L;
      }
      cfg.set("Spieler." + PlayerUtil.getUUID(Spielername) + ".Spielername", Spielername);
      cfg.set("Spieler." + PlayerUtil.getUUID(Spielername) + ".Ban", Boolean.valueOf(true));
      cfg.set("Spieler." + PlayerUtil.getUUID(Spielername) + ".Grund", Grund);
      cfg.set("Spieler." + PlayerUtil.getUUID(Spielername) + ".von", von);
      cfg.set("Spieler." + PlayerUtil.getUUID(Spielername) + ".Ende", Long.valueOf(end));
      Files.saveBanFile();
      ProxiedPlayer target = BungeeCord.getInstance().getPlayer(Spielername);
      if (target != null) {
        target.disconnect(getBannedMessage(Spielername));
      }
      ArrayList<String> banned = cfg.getStringList("GebannteSpieler") != null ? (ArrayList)cfg.getStringList("GebannteSpieler") : new ArrayList();
      banned.add(Spielername);
      cfg.set("GebannteSpieler", banned);
      Files.saveBanFile();
      for (ProxiedPlayer o : BungeeCord.getInstance().getPlayers()) {
        if (o.hasPermission("System.kick"))
        {
          o.sendMessage(String.valueOf(Data.prefix) + "§c" + Spielername + "§7 wurde vom §cNetzwerk §7Gesperrt.");
          o.sendMessage(String.valueOf(Data.prefix) + "§7Grund: §c" + Grund);
          o.sendMessage(String.valueOf(Data.prefix) + "§7Von: §c" + von);
          o.sendMessage(String.valueOf(Data.prefix) + "§7Länge: §c" + getRemainingTime(Spielername));
        }
      }
    }
  }
  
  public static void unBan(String Spielername, String von)
  {
    if (isBanned(Spielername))
    {
      cfg.set("Spieler." + PlayerUtil.getUUID(Spielername) + ".Spielername", Spielername);
      cfg.set("Spieler." + PlayerUtil.getUUID(Spielername) + ".Ban", Boolean.valueOf(false));
      cfg.set("Spieler." + PlayerUtil.getUUID(Spielername) + ".Grund", "");
      cfg.set("Spieler." + PlayerUtil.getUUID(Spielername) + ".von", "");
      cfg.set("Spieler." + PlayerUtil.getUUID(Spielername) + ".Ende", Integer.valueOf(0));
      Files.saveBanFile();
      List Ban2 = cfg.getStringList("GebannteSpieler");
      Ban2.remove(Spielername);
      cfg.set("GebannteSpieler", Ban2);
      Files.saveBanFile();
      for (ProxiedPlayer o : BungeeCord.getInstance().getPlayers()) {
        if (o.hasPermission("System.Kick")) {
          o.sendMessage(String.valueOf(Data.prefix) + "§7Der Spieler §b" + Spielername + " §7wurde von §c" + von + " §7entbannt.");
        }
      }
    }
  }
  
  public static List<String> getBannedPlayers()
  {
    return cfg.getStringList("GebannteSpieler");
  }
  
  public static String getReason(String Spielername)
  {
    String Grund = "";
    if (isBanned(Spielername)) {
      Grund = cfg.getString("Spieler." + PlayerUtil.getUUID(Spielername) + ".Grund");
    }
    return Grund;
  }
  
  public static String getWhoBanned(String Spielername)
  {
    String whobanned = "";
    if (isBanned(Spielername)) {
      whobanned = cfg.getString("Spieler." + PlayerUtil.getUUID(Spielername) + ".von");
    }
    return whobanned;
  }
  
  public static void addtoList(String Spielername, String Grund)
  {
    cfgi.set("BereitsGebannt." + PlayerUtil.getUUID(Spielername), "true");
    cfgi.set("BanGrund." + PlayerUtil.getUUID(Spielername), Grund);
  }
  
  public static boolean getfromlist(String Spielername)
  {
    boolean bool = false;
    if (cfgi.equals("BereitsGebannt." + PlayerUtil.getUUID(Spielername)))
    {
      if (cfgi.get("BereitsGebannt." + PlayerUtil.getUUID(Spielername)).equals("true")) {
        bool = true;
      }
    }
    else
    {
      addtoList(Spielername, "Grundi");
      bool = false;
    }
    return bool;
  }
  
  public static String getReasonfromlast(String Spielername)
  {
    String bool = "Unbekannt";
    bool = cfgi.getString("BanGrund." + PlayerUtil.getUUID(Spielername));
    return bool;
  }
  
  public static long getEnd(String Spielername)
  {
    long end = -1L;
    if (isBanned(Spielername)) {
      end = cfg.getLong("Spieler." + PlayerUtil.getUUID(Spielername) + ".Ende");
    }
    return end;
  }
  
  public static String getRemainingTime(String Spielername)
  {
    String remainingTime = "";
    if (isBanned(Spielername))
    {
      long current = System.currentTimeMillis();
      long end = getEnd(Spielername);
      long difference = end - current;
      if (end == -1L) {
        return "§4Permanent";
      }
      int Sekunden = 0;
      int Minuten = 0;
      int Stunden = 0;
      int Tage = 0;
      while (difference >= 1000L)
      {
        difference -= 1000L;
        Sekunden++;
      }
      while (Sekunden >= 60)
      {
        Sekunden -= 60;
        Minuten++;
      }
      while (Minuten >= 60)
      {
        Minuten -= 60;
        Stunden++;
      }
      while (Stunden >= 24)
      {
        Stunden -= 24;
        Tage++;
      }
      remainingTime = "§b" + Tage + " Tag(e), " + Stunden + " Stunde(n), " + Minuten + " Minute(n) " + Sekunden + " Sekunden";
    }
    return remainingTime;
  }
  
  public static String getBannedMessage(String Spielername)
  {
    String BanMsg = "";
    if (isBanned(Spielername)) {
      BanMsg = "§7Du wurdest vom §3ClazeGames.net Servernetzwerk §7gebannt. \n §7Grund: §a" + getReason(Spielername) + " \n \n §7Verbleibende Zeit: §a  \n" + getRemainingTime(Spielername) + "\n \n §7Unrechter Ban? Stelle einen §3Entbannungsantrag§7 im Forum: \n §3ClazeGames.net/Forum  \n §7oder auf dem TeamSpeak:  \n §3ClazeGames.net";
    }
    return BanMsg;
  }
}
