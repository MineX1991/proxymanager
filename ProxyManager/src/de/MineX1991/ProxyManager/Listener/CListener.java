package de.MineX1991.ProxyManager.Listener;

import de.MineX1991.ProxyManager.Lobby.Hu;
import de.MineX1991.ProxyManager.MOTD.Config;
import de.MineX1991.ProxyManager.Main.Data;
import de.MineX1991.ProxyManager.Main.Main;
import de.MineX1991.ProxyManager.Manager.MuteManager;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.scheduler.TaskScheduler;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import net.md_5.bungee.event.EventHandler;

public class CListener
  implements Listener
{
  public static ArrayList<ProxiedPlayer> list = new ArrayList();
  
  @EventHandler
  public void onsecond(PostLoginEvent e)
  {
    if ((ProxyServer.getInstance().getOnlineCount() > Config.Config.MaxPlayers.intValue()) && (!e.getPlayer().hasPermission("bungeecord.premiumlobby"))) {
      e.getPlayer().disconnect(String.valueOf(Data.prefix) + "§7Das maximale §cSpielerLimit §7von §c" + Config.Config.MaxPlayers + "§7 ist erreicht. \n §c \n §7Nur noch §cSpieler §7mit dem §cPremiumRang §7können noch §cJoinen§7! \n §7TeamSpeak: §cTryRusher.net");
    }
  }
  
  @EventHandler
  public void onPostLogin(PostLoginEvent event)
  {
    final ProxiedPlayer p = event.getPlayer();
    Hu.send(p);
    ProxyServer.getInstance().getScheduler().schedule(Main.plugin, new Runnable()
    {
      public void run()
      {
        if (Main.playerconf.get("Spieler" + p.getName()) == null)
        {
          Main.playerconf.set("Spieler" + p.getName(), p.getName());
          Main.playerconf.set("Amount", Integer.valueOf(Main.playerconf.getInt("Amount") + 1));
          for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers())
          {
            all.sendMessage("§3§m§l-------------------------------------------");
            all.sendMessage(" ");
            all.sendMessage(" ");
            all.sendMessage(String.valueOf(Data.prefix) + "§e" + p.getName() + "§7 ist neu auf §3TryRusher.de§7! §8§l[ §c§l#" + Main.playerconf.getInt("Amount") + "§8§l ]");
            all.sendMessage(" ");
            all.sendMessage(" ");
            all.sendMessage("§3§m§l-------------------------------------------");
          }
          try
          {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(Main.playerconf, Main.playerfile);
          }
          catch (IOException localIOException) {}
        }
      }
    }, 2L, TimeUnit.SECONDS);
  }
  
  @EventHandler
  public void onChat(ChatEvent e)
  {
    ProxiedPlayer ps = (ProxiedPlayer)e.getSender();
    if (e.getMessage().equalsIgnoreCase("/gold"))
    {
      e.setCancelled(true);
      ps.sendMessage("§7§l§m-----------------------------------------");
      ps.sendMessage(" ");
      ps.sendMessage(String.valueOf(Data.prefix) + "§7Besuche die Seite: §chttp://TryRusher.de/gold");
      ps.sendMessage(" ");
      ps.sendMessage("§7§l§m-----------------------------------------");
    }
    if (e.getMessage().equalsIgnoreCase("/hero"))
    {
      e.setCancelled(true);
      ps.sendMessage("§7§l§m-----------------------------------------");
      ps.sendMessage(" ");
      ps.sendMessage(String.valueOf(Data.prefix) + "§7Besuche die Seite: §chttp://TryRusher.de/hero");
      ps.sendMessage(" ");
      ps.sendMessage("§7§l§m-----------------------------------------");
    }
    if (e.getMessage().equalsIgnoreCase("/ultra"))
    {
      e.setCancelled(true);
      ps.sendMessage("§7§l§m-----------------------------------------");
      ps.sendMessage(" ");
      ps.sendMessage(String.valueOf(Data.prefix) + "§7Besuche die Seite: §chttp://TryRusher.de/ultra");
      ps.sendMessage(" ");
      ps.sendMessage("§7§l§m-----------------------------------------");
    }
    if (e.getMessage().startsWith("/aac"))
    {
      ProxiedPlayer p = (ProxiedPlayer)e.getSender();
      p.sendMessage("Unknown command. Type '/help' for help.");
      e.setCancelled(true);
      return;
    }
    if (e.getMessage().startsWith("/aac:aac"))
    {
      ProxiedPlayer p = (ProxiedPlayer)e.getSender();
      p.sendMessage("Unknown command. Type '/help' for help.");
      e.setCancelled(true);
      return;
    }
    if (e.getMessage().startsWith("/AAC:AAC"))
    {
      ProxiedPlayer p = (ProxiedPlayer)e.getSender();
      p.sendMessage("Unknown command. Type '/help' for help.");
      e.setCancelled(true);
      return;
    }
    if (e.getMessage().startsWith("/AAC"))
    {
      ProxiedPlayer p = (ProxiedPlayer)e.getSender();
      p.sendMessage("Unknown command. Type '/help' for help.");
      e.setCancelled(true);
      return;
    }
    final ProxiedPlayer p = (ProxiedPlayer)e.getSender();
    if ((!e.getMessage().startsWith("/")) && (!p.hasPermission("bungeecord.command.ban"))) {
      if (list.contains(p))
      {
        e.setCancelled(true);
        p.sendMessage(String.valueOf(Data.prefix) + "§cBitte warte einen Moment... Verdacht auf Spam!");
      }
      else
      {
        list.add(p);
        ProxyServer.getInstance().getScheduler().schedule(Main.plugin, new Runnable()
        {
          public void run()
          {
            CListener.list.remove(p);
          }
        }, 3L, TimeUnit.SECONDS);
      }
    }
    String msg2 = e.getMessage();
    if (MuteManager.isMuted(p.getName()))
    {
      long current = System.currentTimeMillis();
      long end = MuteManager.getEnd(p.getName());
      if ((end < current) && (end != -1L))
      {
        MuteManager.unMute(p.getName(), "Automatische Cloud");
        e.setCancelled(false);
      }
      else if (!e.getMessage().startsWith("/"))
      {
        e.setCancelled(true);
        p.sendMessage(String.valueOf(Data.prefix) + "§7Derzeit ist der Chat für dich Deaktiviert!");
        p.sendMessage(String.valueOf(Data.prefix) + "§7Grund: §c" + MuteManager.getReason(p.getName()));
        p.sendMessage(String.valueOf(Data.prefix) + "§7Verbleibende Zeit: §c" + MuteManager.getRemainingTime(p.getName()));
      }
    }
  }
}
