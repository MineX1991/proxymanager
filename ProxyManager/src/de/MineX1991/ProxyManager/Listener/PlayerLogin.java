package de.MineX1991.ProxyManager.Listener;

import de.MineX1991.ProxyManager.MOTD.Config;
import de.MineX1991.ProxyManager.Main.Main;
import de.MineX1991.ProxyManager.Manager.BanManager;
import de.MineX1991.ProxyManager.Manager.MuteManager;
import java.util.concurrent.TimeUnit;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.PendingConnection;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.event.ServerDisconnectEvent;
import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.scheduler.BungeeScheduler;

public class PlayerLogin
  implements Listener
{
  @EventHandler
  public void onLogin(LoginEvent e)
  {
    String playername = e.getConnection().getName();
    BanManager.createPlayer(playername);
    MuteManager.createPlayer(playername);
    if (BanManager.isBanned(playername))
    {
      long current = System.currentTimeMillis();
      long end = BanManager.getEnd(playername);
      if ((end > current) || (end == -1L))
      {
        e.setCancelled(true);
        e.setCancelReason(BanManager.getBannedMessage(playername));
      }
      else
      {
        e.setCancelled(false);
        BanManager.unBan(playername, "Automatische Cloud");
      }
    }
  }
  
  @EventHandler
  public void on(ServerConnectEvent e)
  {
    try
    {
      for (ProxiedPlayer all : BungeeCord.getInstance().getPlayers())
      {
        String header = "§7\n §7» §aTryRusher.de §8✘ §6Dein §6Netzwerk §7« \n §6Derzeit spielen§a " + BungeeCord.getInstance().getOnlineCount() + "/" + Config.Config.MaxPlayers + " §6Spieler \n §7";
        String footer = "§7\n §aServer §8» §6" + all.getServer().getInfo().getName() + " §8\n §aTeamSpeak §8» §6TryRusher.de§8 \n §aForum §8» §6www.TryRusher.de \n§7";
        all.setTabHeader(new TextComponent(header), new TextComponent(footer));
      }
    }
    catch (Exception localException) {}
  }
  
  @EventHandler
  public void on(ServerDisconnectEvent e)
  {
    try
    {
      for (ProxiedPlayer all : BungeeCord.getInstance().getPlayers())
      {
        String header = "§7\n §7» §aTryRusher.de §8✘ §6Dein §6Netzwerk §7« \n §6Derzeit spielen§a " + BungeeCord.getInstance().getOnlineCount() + "/" + Config.Config.MaxPlayers + " §6Spieler \n §7";
        String footer = "§7\n §aServer §8» §6" + all.getServer().getInfo().getName() + " §8\n §aTeamSpeak §8» §6TryRusher.de§8 \n §aForum §8» §6www.TryRusher.de \n§7";
        all.setTabHeader(new TextComponent(header), new TextComponent(footer));
      }
    }
    catch (Exception localException) {}
  }
  
  @EventHandler
  public void on(ServerSwitchEvent e)
  {
    String server = e.getPlayer().getServer().getInfo().getName();
    try
    {
      BungeeCord.getInstance().getScheduler().schedule(Main.plugin, new Runnable()
      {
        public void run()
        {
          for (ProxiedPlayer all : BungeeCord.getInstance().getPlayers())
          {
            String header = "§7\n §7» §aTryRusher.de §8✘ §6Dein §6Netzwerk §7« \n §6Derzeit spielen§a " + BungeeCord.getInstance().getOnlineCount() + "/" + Config.Config.MaxPlayers + " §6Spieler \n §7";
            String footer = "§7\n §aServer §8» §6" + all.getServer().getInfo().getName() + " §8\n §aTeamSpeak §8» §6TryRusher.de§8 \n §aForum §8» §6www.TryRusher.de \n§7";
            all.setTabHeader(new TextComponent(header), new TextComponent(footer));
          }
        }
      }, 15L, TimeUnit.MILLISECONDS);
    }
    catch (Exception localException) {}
  }
}
