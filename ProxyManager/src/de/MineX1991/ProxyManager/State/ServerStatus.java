package de.MineX1991.ProxyManager.State;

import java.util.List;

public class ServerStatus
{
  private String description;
  private Players players;
  private Version version;
  private String favicon;
  private int time;
  
  public int hashCode()
  {
    int PRIME = 59;
    int result = 1;
    String $description = getDescription();
    result = result * 59 + ($description == null ? 0 : $description.hashCode());
    Players $players = getPlayers();
    result = result * 59 + ($players == null ? 0 : $players.hashCode());
    Version $version = getVersion();
    result = result * 59 + ($version == null ? 0 : $version.hashCode());
    String $favicon = getFavicon();
    result = result * 59 + ($favicon == null ? 0 : $favicon.hashCode());
    result = result * 59 + getTime();
    return result;
  }
  
  public boolean canEqual(Object other)
  {
    return other instanceof ServerStatus;
  }
  
  public boolean equals(Object o)
  {
    if (o == this) {
      return true;
    }
    if (!(o instanceof ServerStatus)) {
      return false;
    }
    ServerStatus other = (ServerStatus)o;
    if (!other.canEqual(this)) {
      return false;
    }
    String this$description = getDescription();
    String other$description = other.getDescription();
    if (this$description == null ? other$description != null : !this$description.equals(other$description)) {
      return false;
    }
    Players this$players = getPlayers();
    Players other$players = other.getPlayers();
    if (this$players == null ? other$players != null : !this$players.equals(other$players)) {
      return false;
    }
    Version this$version = getVersion();
    Version other$version = other.getVersion();
    if (this$version == null ? other$version != null : !this$version.equals(other$version)) {
      return false;
    }
    String this$favicon = getFavicon();
    String other$favicon = other.getFavicon();
    if (this$favicon == null ? other$favicon != null : !this$favicon.equals(other$favicon)) {
      return false;
    }
    if (getTime() == other.getTime()) {
      return true;
    }
    return false;
  }
  
  public String toString()
  {
    return "StatusResponse(description=" + getDescription() + ", players=" + getPlayers() + ", version=" + getVersion() + ", favicon=" + getFavicon() + ", time=" + getTime() + ")";
  }
  
  public String getDescription()
  {
    return this.description;
  }
  
  public void setDescription(String description)
  {
    this.description = description;
  }
  
  public Players getPlayers()
  {
    return this.players;
  }
  
  public void setPlayers(Players players)
  {
    this.players = players;
  }
  
  public Version getVersion()
  {
    return this.version;
  }
  
  public void setVersion(Version version)
  {
    this.version = version;
  }
  
  public String getFavicon()
  {
    return this.favicon;
  }
  
  public void setFavicon(String favicon)
  {
    this.favicon = favicon;
  }
  
  public void setTime(int time)
  {
    this.time = time;
  }
  
  public int getTime()
  {
    return this.time;
  }
  
  public class Player
  {
    private String name;
    private String id;
    
    public Player() {}
    
    public boolean equals(Object o)
    {
      if (o == this) {
        return true;
      }
      if (!(o instanceof Player)) {
        return false;
      }
      Player other = (Player)o;
      if (!other.canEqual(this)) {
        return false;
      }
      String this$name = getName();
      String other$name = other.getName();
      if (this$name == null ? other$name != null : !this$name.equals(other$name)) {
        return false;
      }
      String this$id = getId();
      String other$id = other.getId();
      return other$id == null ? true : this$id == null ? false : this$id.equals(other$id);
    }
    
    public int hashCode()
    {
      int PRIME = 59;
      int result = 1;
      String $name = getName();
      result = result * 59 + ($name == null ? 0 : $name.hashCode());
      String $id = getId();
      result = result * 59 + ($id == null ? 0 : $id.hashCode());
      return result;
    }
    
    public boolean canEqual(Object other)
    {
      return other instanceof Player;
    }
    
    public String toString()
    {
      return "StatusResponse.Player(name=" + getName() + ", id=" + getId() + ")";
    }
    
    public void setName(String name)
    {
      this.name = name;
    }
    
    public String getName()
    {
      return this.name;
    }
    
    public void setId(String id)
    {
      this.id = id;
    }
    
    public String getId()
    {
      return this.id;
    }
  }
  
  public class Players
  {
    private int max;
    private int online;
    private List<ServerStatus.Player> sample;
    
    public Players() {}
    
    public boolean canEqual(Object other)
    {
      return other instanceof Players;
    }
    
    public boolean equals(Object o)
    {
      if (o == this) {
        return true;
      }
      if (!(o instanceof Players)) {
        return false;
      }
      Players other = (Players)o;
      if (!other.canEqual(this)) {
        return false;
      }
      if (getMax() != other.getMax()) {
        return false;
      }
      if (getOnline() != other.getOnline()) {
        return false;
      }
      List<ServerStatus.Player> this$sample = getSample();
      List<ServerStatus.Player> other$sample = other.getSample();
      return other$sample == null ? true : this$sample == null ? false : this$sample.equals(other$sample);
    }
    
    public int hashCode()
    {
      int PRIME = 59;
      int result = 1;
      result = result * 59 + getMax();
      result = result * 59 + getOnline();
      List<ServerStatus.Player> $sample = getSample();
      result = result * 59 + ($sample == null ? 0 : $sample.hashCode());
      return result;
    }
    
    public String toString()
    {
      return "StatusResponse.Players(max=" + getMax() + ", online=" + getOnline() + ", sample=" + getSample() + ")";
    }
    
    public void setMax(int max)
    {
      this.max = max;
    }
    
    public int getMax()
    {
      return this.max;
    }
    
    public void setOnline(int online)
    {
      this.online = online;
    }
    
    public int getOnline()
    {
      return this.online;
    }
    
    public List<ServerStatus.Player> getSample()
    {
      return this.sample;
    }
    
    public void setSample(List<ServerStatus.Player> sample)
    {
      this.sample = sample;
    }
  }
  
  public class Version
  {
    private String name;
    private String protocol;
    
    public Version() {}
    
    public boolean equals(Object o)
    {
      if (o == this) {
        return true;
      }
      if (!(o instanceof Version)) {
        return false;
      }
      Version other = (Version)o;
      if (!other.canEqual(this)) {
        return false;
      }
      String this$name = getName();
      String other$name = other.getName();
      if (this$name == null ? other$name != null : !this$name.equals(other$name)) {
        return false;
      }
      String this$protocol = getProtocol();
      String other$protocol = other.getProtocol();
      return other$protocol == null ? true : this$protocol == null ? false : this$protocol.equals(other$protocol);
    }
    
    public int hashCode()
    {
      int PRIME = 59;
      int result = 1;
      String $name = getName();
      result = result * 59 + ($name == null ? 0 : $name.hashCode());
      String $protocol = getProtocol();
      result = result * 59 + ($protocol == null ? 0 : $protocol.hashCode());
      return result;
    }
    
    public boolean canEqual(Object other)
    {
      return other instanceof Version;
    }
    
    public String toString()
    {
      return "StatusResponse.Version(name=" + getName() + ", protocol=" + getProtocol() + ")";
    }
    
    public void setName(String name)
    {
      this.name = name;
    }
    
    public String getName()
    {
      return this.name;
    }
    
    public void setProtocol(String protocol)
    {
      this.protocol = protocol;
    }
    
    public String getProtocol()
    {
      return this.protocol;
    }
  }
}
