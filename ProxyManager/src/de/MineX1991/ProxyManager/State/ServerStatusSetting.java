package de.MineX1991.ProxyManager.State;

public class ServerStatusSetting
{
  private String status;
  private Integer max;
  private Integer online;
  
  public ServerStatusSetting(String ServerStatus2, Integer OnlinePlayers, Integer MaxPlayers)
  {
    this.status = ServerStatus2;
    this.max = MaxPlayers;
  }
  
  public Integer getMaxPlayers()
  {
    return this.max;
  }
  
  public Integer getOnlinePlayers()
  {
    return this.online;
  }
  
  public String getServerStatus()
  {
    return this.status;
  }
}
