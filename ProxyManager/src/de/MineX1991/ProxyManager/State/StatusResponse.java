package de.MineX1991.ProxyManager.State;

import net.md_5.bungee.api.config.ServerInfo;

public class StatusResponse
{
  private ServerInfo _server;
  private String _ServerMotd;
  private Integer _OnlinePlayers;
  private Integer _MaxPlayers;
  private Boolean _isOnline;
  
  public StatusResponse(ServerInfo Info, Boolean isOnline, String ServerMotd, Integer OnlinePlayers, Integer MaxPlayers)
  {
    this._server = Info;
    this._isOnline = isOnline;
    this._ServerMotd = ServerMotd;
    this._OnlinePlayers = OnlinePlayers;
    this._MaxPlayers = MaxPlayers;
  }
  
  public ServerInfo getServerInfo()
  {
    return this._server;
  }
  
  public String getServerMotd()
  {
    return this._ServerMotd;
  }
  
  public Integer getMaxPlayers()
  {
    return this._MaxPlayers;
  }
  
  public Integer getOnlinePlayers()
  {
    return this._OnlinePlayers;
  }
  
  public Boolean isOnline()
  {
    return this._isOnline;
  }
}
