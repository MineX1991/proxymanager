package de.MineX1991.ProxyManager.Main;

import de.MineX1991.ProxyManager.Commands.Check;
import de.MineX1991.ProxyManager.Commands.Strafe;
import de.MineX1991.ProxyManager.Commands.TempBan;
import de.MineX1991.ProxyManager.Commands.UnBan;
import de.MineX1991.ProxyManager.Commands.broadcast;
import de.MineX1991.ProxyManager.Listener.CListener;
import de.MineX1991.ProxyManager.Listener.PlayerLogin;
import de.MineX1991.ProxyManager.MOTD.Config;
import static de.MineX1991.ProxyManager.Manager.BanManager.Ban;
import de.MineX1991.ProxyManager.Manager.Files;
import de.MineX1991.ProxyManager.Ranks.Database;
import de.MineX1991.ProxyManager.Commands.*;
import de.MineX1991.ProxyManager.Listener.*;
import de.MineX1991.ProxyManager.Warns.*;
import de.MineX1991.ProxyManager.MOTD.*;
import de.MineX1991.ProxyManager.Ranks.*;
import de.MineX1991.ProxyManager.StrikeSystem.*;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import de.MineX1991.ProxyManager.Commands.BanList;
import de.MineX1991.ProxyManager.Lobby.Hub_Command;
import de.MineX1991.ProxyManager.Lobby.L_Command;
import de.MineX1991.ProxyManager.Lobby.Leave_Command;
import de.MineX1991.ProxyManager.Lobby.Lobby_Command;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;
import net.md_5.bungee.api.scheduler.TaskScheduler;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public class Main
  extends Plugin
{
  public static boolean updateAvailable;
  PluginManager pm = BungeeCord.getInstance().getPluginManager();
  public static Main plugin;
  private java.util.List<String> wort = new ArrayList();
  private java.util.List<String> ads = new ArrayList();
  private java.util.List<String> plugins = new ArrayList();
  public static Database dbConfig = null;
  public static int intege = 0;
  public static File playerfile;
  public static Configuration playerconf;
  
  public void onEnable()
  {
    plugin = this;
    Config.LoadConfig(this);
    this.plugins.addAll(Arrays.asList(new String[] { "/plugins", "/demote", "/promote", "/permissionsex:", "/pex", "/pl", "/?", "/help", "/op", "/bungee", "/me", "/bukkit:plugins", "/bukkit:pl", "/bukkit:?", "/bukkit:help", "/bukkit:me", "/version", "/tell", "/me", "/bungee", "/op", "/minecraft:me" }));
    this.wort.addAll(Arrays.asList(new String[] { "noob", "l2p", "ficken", "hurensohn", "missgeburt", "bastard", "basti", "spast", "spasti", "huren", "huso", "l2p" }));
    try
    {
      createFolders();
    }
    catch (Exception localException) {}
    Files.setDefaultConfig();
    try
    {
      if (!getDataFolder().exists()) {
        getDataFolder().mkdir();
      }
      if (!(Main.playerfile = new File(getDataFolder().getPath(), "playerconf.yml")).exists())
      {
        playerfile.createNewFile();
        playerconf = ConfigurationProvider.getProvider(YamlConfiguration.class).load(playerfile);
        playerconf.set("Amount", Integer.valueOf(0));
        ConfigurationProvider.getProvider(YamlConfiguration.class).save(playerconf, playerfile);
      }
      playerconf = ConfigurationProvider.getProvider(YamlConfiguration.class).load(playerfile);
    }
    catch (IOException e)
    {
      System.err.println("Error not create File config.yml");
      return;
    }
    this.pm.registerListener(this, new PlayerLogin());
    this.pm.registerCommand(this, new broadcast());
    this.pm.registerCommand(this, new Ban());
    this.pm.registerCommand(this, new Strafe());
    this.pm.registerCommand(this, new UnBan());
    this.pm.registerCommand(this, new TempBan());
    this.pm.registerCommand(this, new BanList());
    this.pm.registerCommand(this, new Check());
    this.pm.registerListener(this, new CListener());
    this.pm.registerCommand(this, new Mute());
    this.pm.registerCommand(this, new UnMute());
    this.pm.registerCommand(this, new TempMute());
    this.pm.registerCommand(this, new MuteList());
    this.pm.registerCommand(this, new Rejoin());
    this.pm.registerCommand(this, new YouTuber());
    this.pm.registerCommand(this, new Ping());
    this.pm.registerCommand(this, new Globalmute());
    this.pm.registerCommand(this, new ClearChat());
    this.pm.registerCommand(this, new Me());
    this.pm.registerCommand(this, new Kick());
    this.pm.registerCommand(this, new restart_Command("restart"));
    this.pm.registerCommand(this, new Wartungs_Command("wartungen"));
    this.pm.registerCommand(this, new StrikeCMD("strike"));
    this.pm.registerCommand(this, new SrtikesCMD("strikes"));
    this.pm.registerCommand(this, new DelStrikeCMD("delstrike"));
    this.pm.registerCommand(this, new ShowStrikesCMD("showstrikes"));
    this.pm.registerCommand(this, new TeamChat("tc"));
    this.pm.registerCommand(this, new Command_Motd("motd"));
    this.pm.registerCommand(this, new Command_SetMax("setmax"));
    this.pm.registerCommand(this, new Command_Rank("rechte"));
    this.pm.registerCommand(this, new Command_Warn("jtzjktfkkgvkgvkj"));
    this.pm.registerCommand(this, new Command_Warns("warns"));
    this.pm.registerCommand(this, new Command_WerbungList("werbung"));
    this.pm.registerCommand(this, new Hub_Command());
    this.pm.registerCommand(this, new L_Command());
    this.pm.registerCommand(this, new Leave_Command());
    this.pm.registerCommand(this, new Support("tk"));
    this.pm.registerCommand(this, new Lobby_Command());
    this.pm.registerListener(this, new ChatListener());
    this.pm.registerListener(this, new Listener_Motds());
    this.pm.registerListener(this, new Manager_Chat());
    this.pm.registerCommand(this, new JoinMe("joinme"));
    this.pm.registerCommand(this, new ServerJoinCommand("xyserverxyx"));
    this.pm.registerCommand(this, new Report());
    this.pm.registerCommand(this, new Reports());
    
    Calendar date = Calendar.getInstance();
    date.set(10, 0);
    date.set(12, 0);
    date.set(13, 0);
    date.set(14, 0);
    
    ProxyServer.getInstance().getScheduler().schedule(this, new Runnable()
    {
      public void run()
      {
        Main.intege = ++Main.intege;
        if (Main.intege == 0) {
          for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers())
          {
            all.sendMessage(" ");
            all.sendMessage("          §7Unsere §aTeamSpeak§7 IP:");
            all.sendMessage("               §aTryRusher.de");
            all.sendMessage(" ");
          }
        }
        if (Main.intege == 1) {
          for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers())
          {
            all.sendMessage(" ");
            all.sendMessage("          §7Interesse am §5YouTuber §7Rang?");
            all.sendMessage("                  §a/youtuber");
            all.sendMessage(" ");
          }
        }
        if (Main.intege == 2) {
          for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers())
          {
            all.sendMessage(" ");
            all.sendMessage("           §7Unser §aForum §7findest du unter:");
            all.sendMessage("                  §awww.TryRusher.de");
            all.sendMessage(" ");
          }
        }
        if (Main.intege == 3) {
          for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers())
          {
            all.sendMessage(" ");
            all.sendMessage("           §7Interesse an einem Rang (§6Gold§7,§3Hero§7,§bUltra§7)?");
            all.sendMessage("                   §7Besuche §ahttp://shop.TryRusher.de");
            all.sendMessage(" ");
          }
        }
      }
    }, 0L, 5L, TimeUnit.MINUTES);
    Listener_Motds.LoadMotds();
    BungeeCord.getInstance().getConsole().sendMessage("BungeeCordSystem wurde geladen");
  }
  
  public void onDisable()
  {
    BungeeCord.getInstance().getConsole().sendMessage("BungeeCordSystem wurde deaktiviert.");
  }
  
  public void createFolders()
  {
    if (!getDataFolder().exists()) {
      getDataFolder().mkdir();
    }
    if (!(Files.BanFile = new File(getDataFolder().getPath(), "bans.yml")).exists()) {
      try
      {
        Files.BanFile.createNewFile();
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
    }
    if (!(Files.MuteFile = new File(getDataFolder().getPath(), "mutes.yml")).exists()) {
      try
      {
        Files.MuteFile.createNewFile();
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
    }
    if (!(Files.MsgFile = new File(getDataFolder().getPath(), "strike.yml")).exists()) {
      try
      {
        Files.MsgFile.createNewFile();
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
    }
    try
    {
      Files.MuteConfig = ConfigurationProvider.getProvider(YamlConfiguration.class).load(Files.MuteFile);
    }
    catch (IOException e1)
    {
      e1.printStackTrace();
    }
    try
    {
      Files.BanConfig = ConfigurationProvider.getProvider(YamlConfiguration.class).load(Files.BanFile);
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
    try
    {
      Files.MessagesConfiguration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(Files.MsgFile);
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
  }
  
  public java.util.List<String> getAds()
  {
    return this.ads;
  }
  
  public java.util.List<String> getPlugins()
  {
    return this.plugins;
  }
  
  public java.util.List<String> getWort()
  {
    return this.wort;
  }
}
