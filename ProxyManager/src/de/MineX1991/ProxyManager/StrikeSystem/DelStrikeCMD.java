package de.MineX1991.ProxyManager.StrikeSystem;

import de.MineX1991.ProxyManager.Main.Data;
import de.MineX1991.ProxyManager.Manager.Files;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.config.Configuration;

public class DelStrikeCMD
  extends Command
{
  static Configuration cfg = Files.MessagesConfiguration;
  
  public DelStrikeCMD(String name)
  {
    super(name);
  }
  
  public void execute(CommandSender sender, String[] args)
  {
    if (args.length == 1)
    {
      ProxiedPlayer p = ProxyServer.getInstance().getPlayer(args[0]);
      if (sender.hasPermission("tryrusher.strikesystem"))
      {
        int i = cfg.getInt(p.getUniqueId() + ".Strikes");
        if (i == 0)
        {
          cfg.set(p.getUniqueId() + ".Strikes", null);
          cfg.set(p.getUniqueId() + ".Grund1", null);
          cfg.set(p.getUniqueId() + ".Grund2", null);
          cfg.set(p.getUniqueId() + ".Grund3", null);
          cfg.set(p.getUniqueId() + ".Grund4", null);
          cfg.set(p.getUniqueId() + ".Grund5", null);
          
          Files.saveMessagesConfiguration();
          sender.sendMessage(Data.prefix + "§cDer Spieler hat keine Strikes!");
          return;
        }
        if (i == 1)
        {
          cfg.set(p.getUniqueId() + ".Strikes", null);
          cfg.set(p.getUniqueId() + ".Grund1", null);
          cfg.set(p.getUniqueId() + ".Grund2", null);
          cfg.set(p.getUniqueId() + ".Grund3", null);
          cfg.set(p.getUniqueId() + ".Grund4", null);
          cfg.set(p.getUniqueId() + ".Grund5", null);
          Files.saveMessagesConfiguration();
          sender.sendMessage(Data.prefix + "§3Dem Spieler §6" + p.getName() + "§3 wurde ein Strike entzogen §4[#" + i + "]");
          p.sendMessage(Data.prefix + "§3Dir wurde ein Strike entzogen! §4[#" + i + "]");
          return;
        }
        if (i == 2)
        {
          cfg.set(p.getUniqueId() + ".Strikes", Integer.valueOf(1));
          cfg.set(p.getUniqueId() + ".Grund2", "Unbekannt");
          Files.saveMessagesConfiguration();
          sender.sendMessage(Data.prefix + "§3Dem Spieler §6" + p.getName() + "§3 wurde ein Strike entzogen §4[#" + i + "]");
          p.sendMessage(Data.prefix + "§3Dir wurde ein Strike entzogen! §4[#" + i + "]");
          return;
        }
        if (i == 3)
        {
          cfg.set(p.getUniqueId() + ".Strikes", Integer.valueOf(2));
          cfg.set(p.getUniqueId() + ".Grund3", "Unbekannt");
          Files.saveMessagesConfiguration();
          sender.sendMessage(Data.prefix + "§3Dem Spieler §6" + p.getName() + "§3 wurde ein Strike entzogen §4[#" + i + "]");
          p.sendMessage(Data.prefix + "§3Dir wurde ein Strike entzogen! §4[#" + i + "]");
          return;
        }
        if (i == 4)
        {
          cfg.set(p.getUniqueId() + ".Strikes", Integer.valueOf(3));
          cfg.set(p.getUniqueId() + ".Grund4", "Unbekannt");
          Files.saveMessagesConfiguration();
          sender.sendMessage(Data.prefix + "§3Dem Spieler §6" + p.getName() + "§3 wurde ein Strike entzogen §4[#" + i + "]");
          p.sendMessage(Data.prefix + "§3Dir wurde ein Strike entzogen! §4[#" + i + "]");
          return;
        }
        if (i == 5)
        {
          cfg.set(p.getUniqueId() + ".Strikes", Integer.valueOf(4));
          cfg.set(p.getUniqueId() + ".Grund5", "Unbekannt");
          Files.saveMessagesConfiguration();
          sender.sendMessage(Data.prefix + "§3Dem Spieler §6" + p.getName() + "§3 wurde ein Strike entzogen §4[#" + i + "]");
          p.sendMessage(Data.prefix + "§3Dir wurde ein Strike entzogen! §4[#" + i + "]");
          return;
        }
        if (i == 6)
        {
          cfg.set(p.getUniqueId() + ".Strikes", Integer.valueOf(5));
          Files.saveMessagesConfiguration();
          sender.sendMessage(Data.prefix + "§3Dem Spieler §6" + p.getName() + "§3 wurde ein Strike entzogen §4[#" + i + "]");
          p.sendMessage(Data.prefix + "§3Dir wurde ein Strike entzogen! §4[#" + i + "]");
          return;
        }
        if (i == 7)
        {
          cfg.set(p.getUniqueId() + ".Strikes", Integer.valueOf(5));
          Files.saveMessagesConfiguration();
          sender.sendMessage(Data.prefix + "§3Dem Spieler §6" + p.getName() + "§3 wurde ein Strike entzogen §4[#" + i + "]");
          p.sendMessage(Data.prefix + "§3Dir wurde ein Strike entzogen! §4[#" + i + "]");
          return;
        }
        if (i > 7)
        {
          cfg.set(p.getUniqueId() + ".Strikes", Integer.valueOf(5));
          Files.saveMessagesConfiguration();
          sender.sendMessage(Data.prefix + "§3Dem Spieler §6" + p.getName() + "§3 wurde ein Strike entzogen §4[#" + i + "]");
          p.sendMessage(Data.prefix + "§3Dir wurde ein Strike entzogen! §4[#" + i + "]");
        }
      }
      else
      {
        sender.sendMessage(Data.prefix + "§cDir fehlt die Berechtigung!");
      }
    }
    else
    {
      sender.sendMessage(Data.prefix + "§cBenutzung: §6/DelStrike [Spieler]");
    }
  }
}
