package de.MineX1991.ProxyManager.StrikeSystem;

import de.MineX1991.ProxyManager.Main.Data;
import de.MineX1991.ProxyManager.Manager.BanManager;
import de.MineX1991.ProxyManager.Manager.Files;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.config.Configuration;

public class StrikeCMD
  extends Command
{
  static Configuration cfg = Files.MessagesConfiguration;
  static int i;
  
  public StrikeCMD(String name)
  {
    super(name);
  }
  
  public void execute(CommandSender sender, String[] args)
  {
    if (args.length > 1)
    {
      if (sender.hasPermission("tryrusher.strikesystem"))
      {
        String Player = args[0];
        String Message = "";
        for (int i = 1; i < args.length; i++) {
          Message = Message + " " + args[i];
        }
        String Grund = Message;
        ProxiedPlayer target = ProxyServer.getInstance().getPlayer(Player);
        if (target.hasPermission("tryrusher.getstrike"))
        {
          if (target == null)
          {
            sender.sendMessage(Data.prefix + "§cDer Spieler ist nicht online!");
            return;
          }
          if (cfg.get(target.getUniqueId() + ".Strikes") == null)
          {
            cfg.set(target.getUniqueId() + ".Strikes", Integer.valueOf(1));
            cfg.set(target.getUniqueId() + ".Grund1", Grund);
            cfg.set(target.getUniqueId() + ".Grund2", "Unbekannt");
            cfg.set(target.getUniqueId() + ".Grund3", "Unbekannt");
            cfg.set(target.getUniqueId() + ".Grund4", "Unbekannt");
            cfg.set(target.getUniqueId() + ".Grund5", "Unbekannt");
            cfg.set(target.getUniqueId() + ".Name", target.getName());
            Files.saveMessagesConfiguration();
            target.sendMessage(Data.prefix + "§cDu hast einen §6§lStrike §cerhalten! §8[§b#1§8]");
            sender.sendMessage(Data.prefix + "§6Das Teammitglied §4" + target.getName() + "§6 hat einen Strike erhalten!");
            return;
          }
          i = cfg.getInt(target.getUniqueId() + ".Strikes");
          if (i == 1)
          {
            cfg.set(target.getUniqueId() + ".Grund2", Grund);
            Files.saveMessagesConfiguration();
            i += 1;
          }
          else if (i == 2)
          {
            i += 1;
            cfg.set(target.getUniqueId() + ".Strikes", Integer.valueOf(i + 2));
            cfg.set(target.getUniqueId() + ".Grund3", Grund);
            Files.saveMessagesConfiguration();
          }
          else if (i == 3)
          {
            i += 1;
            cfg.set(target.getUniqueId() + ".Strikes", Integer.valueOf(i + 3));
            cfg.set(target.getUniqueId() + ".Grund4", Grund);
            Files.saveMessagesConfiguration();
          }
          else if (i == 4)
          {
            i += 1;
            cfg.set(target.getUniqueId() + ".Strikes", Integer.valueOf(i + 4));
            cfg.set(target.getUniqueId() + ".Grund5", Grund);
            Files.saveMessagesConfiguration();
          }
          else if (i == 5)
          {
            i += 1;
            cfg.set(target.getUniqueId() + ".Strikes", Integer.valueOf(i + 5));
            cfg.set(target.getUniqueId() + ".Grund6", Grund);
            Files.saveMessagesConfiguration();
            BanManager.Ban(target.getName(), "Strike-Überschreitung", "StrikeSystem", 432000);
          }
          cfg.set(target.getUniqueId() + ".Strikes", Integer.valueOf(i));
          target.sendMessage(Data.prefix + "§cDu hast einen §6§lStrike §cerhalten! §8[§b#" + i + "§8]");
          sender.sendMessage(Data.prefix + "§6Das Teammitglied §4" + target.getName() + "§6 hat einen Strike erhalten!");
        }
        else
        {
          sender.sendMessage(Data.prefix + "§cDu musst der Gruppe von §6" + target.getName() + "§c die Permission geben: §6tryrusher.getStrike");
        }
      }
      else
      {
        sender.sendMessage(Data.prefix + "§cDir fehlt die benötigte Berechtigung!");
      }
    }
    else {
      sender.sendMessage(Data.prefix + "§cFalsche benutzung! §6Nutze: §3/Strike [Spieler] [Grund]");
    }
  }
}
