package de.MineX1991.ProxyManager.StrikeSystem;

import de.MineX1991.ProxyManager.Main.Data;
import de.MineX1991.ProxyManager.Manager.Files;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.config.Configuration;

public class SrtikesCMD
  extends Command
{
  static Configuration cfg = Files.MessagesConfiguration;
  
  public SrtikesCMD(String name)
  {
    super(name);
  }
  
  public void execute(CommandSender sender, String[] args)
  {
    if (args.length == 0)
    {
      if (sender.hasPermission("clazegames.getstrike"))
      {
        String tar = sender.getName();
        ProxiedPlayer p = ProxyServer.getInstance().getPlayer(tar);
        sender.sendMessage("§3§m-----------------------------");
        sender.sendMessage(Data.prefix + "§eStrikes: §4" + cfg.getInt(new StringBuilder().append(p.getUniqueId()).append(".Strikes").toString()));
        sender.sendMessage(Data.prefix + "§eGrund1: §4" + cfg.getString(new StringBuilder().append(p.getUniqueId()).append(".Grund1").toString()));
        sender.sendMessage(Data.prefix + "§eGrund2: §4" + cfg.getString(new StringBuilder().append(p.getUniqueId()).append(".Grund2").toString()));
        sender.sendMessage(Data.prefix + "§eGrund3: §4" + cfg.getString(new StringBuilder().append(p.getUniqueId()).append(".Grund3").toString()));
        sender.sendMessage(Data.prefix + "§eGrund4: §4" + cfg.getString(new StringBuilder().append(p.getUniqueId()).append(".Grund4").toString()));
        sender.sendMessage(Data.prefix + "§eGrund5: §4" + cfg.getString(new StringBuilder().append(p.getUniqueId()).append(".Grund5").toString()));
        sender.sendMessage("§3§m-----------------------------");
      }
      else
      {
        sender.sendMessage(Data.prefix + "§cEs werden keine Informationen über dich gesammelt...");
      }
    }
    else {
      sender.sendMessage("§cBenutzung: §6/Strikes");
    }
  }
}
