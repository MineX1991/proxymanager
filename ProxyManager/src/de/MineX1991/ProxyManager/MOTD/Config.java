package de.MineX1991.ProxyManager.MOTD;

import net.cubespace.Yamler.Config.InvalidConfigurationException;
import net.md_5.bungee.api.ProxyConfig;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;

public class Config
{
  public static Settings Config = null;
  
  public static void LoadConfig(Plugin p)
  {
    Config = null;
    Config = new Settings(p);
    if (Config.MaxPlayers.intValue() == -1)
    {
      Config.MaxPlayers = Integer.valueOf(ProxyServer.getInstance().getConfig().getPlayerLimit());
      try
      {
        Config.save();
      }
      catch (InvalidConfigurationException e)
      {
        e.printStackTrace();
      }
    }
  }
  
  public static boolean SaveConfig()
  {
    try
    {
      Config.save();
      return true;
    }
    catch (InvalidConfigurationException var0) {}
    return false;
  }
}
