package de.MineX1991.ProxyManager.MOTD;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.ServerPing.PlayerInfo;
import net.md_5.bungee.api.ServerPing.Players;
import net.md_5.bungee.api.ServerPing.Protocol;
import net.md_5.bungee.api.connection.PendingConnection;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class Listener_Motds
  implements Listener
{
  public static HashMap<String, String> PlayerData = new HashMap();
  private static ServerPing.PlayerInfo[] OnlineScore = null;
  static String NewLine = System.getProperty("line.separator");
  
  public static void LoadMotds()
  {
    PlayerData.clear();
    if (NewLine.length() == 2) {
      NewLine = NewLine.substring(1, 2);
    }
    OnlineScore = getOnlinePlayers();
  }
  
  private static ServerPing.PlayerInfo[] getOnlinePlayers()
  {
    String s = Config.Config.DefaultMotd;
    while (s.startsWith(" ")) {
      s = s.substring(1);
    }
    while (s.endsWith(" ")) {
      s = s.substring(0, s.length() - 1);
    }
    ServerPing.PlayerInfo[] PlayerInfo2 = { new ServerPing.PlayerInfo(ChatColor.BLACK + " ", ""), new ServerPing.PlayerInfo(ChatColor.BLACK + "  " + ChatColor.WHITE + s.replace("&", "§") + ChatColor.BLACK + "  ", ""), new ServerPing.PlayerInfo(ChatColor.BLACK + " ", "") };
    return PlayerInfo2;
  }
  
  @EventHandler
  public void onEvent(ProxyPingEvent event)
  {
    ServerPing.Players OnlinePlayers = new ServerPing.Players(Config.Config.MaxPlayers.intValue(), ProxyServer.getInstance().getOnlineCount(), event.getResponse().getPlayers().getSample());
    if ((event.getConnection().getVersion() == 4) || (event.getConnection().getVersion() == 5)) {
      OnlinePlayers = new ServerPing.Players(Config.Config.MaxPlayers.intValue(), ProxyServer.getInstance().getOnlineCount(), OnlineScore);
    }
    String ServerImage = "";
    if (event.getResponse().getFavicon() != null) {
      ServerImage = event.getResponse().getFavicon();
    }
    if (Manager_Chat.Wartungen.booleanValue())
    {
      event.setResponse(new ServerPing(new ServerPing.Protocol(ChatColor.RED + "Wartungsmodus " + ChatColor.GRAY + ProxyServer.getInstance().getOnlineCount() + ChatColor.DARK_GRAY + "/" + ChatColor.GRAY + Config.Config.MaxPlayers, 0), OnlinePlayers, String.valueOf(Config.Config.DefaultMotd.replace("&", "§")) + NewLine + ChatColor.DARK_RED + "§7Der §cWartungsmodus§7 ist derzeit aktiviert!", ServerImage));
      return;
    }
    ServerPing.Protocol Version2 = new ServerPing.Protocol("§4§lFalsche Version!", event.getResponse().getVersion().getProtocol());
    if (Config.Config.useMotds.booleanValue()) {
      if (PlayerData.containsKey(event.getConnection().getAddress().getAddress().toString().replace("/", "")))
      {
        String Message = "";
        if (Config.Config.RandomMotds.isEmpty())
        {
          Message = "";
        }
        else
        {
          Random r = new Random();
          Message = (String)Config.Config.RandomMotds.get(r.nextInt(Config.Config.RandomMotds.size()));
        }
        event.setResponse(new ServerPing(Version2, OnlinePlayers, String.valueOf(Config.Config.DefaultMotd.replace("&", "§")) + ChatColor.RESET + NewLine + ChatColor.RESET + Message.replace("&", "§").replace("%player%", (CharSequence)PlayerData.get(event.getConnection().getAddress().getAddress().toString().replace("/", ""))), ServerImage));
      }
      else
      {
        Integer count = Integer.valueOf(0);
        String Message = "%player%";
        if (!Config.Config.RandomMotds.isEmpty()) {
          while ((count.intValue() <= 10) && (Message.contains("%player%")))
          {
            Random r = new Random();
            Message = (String)Config.Config.RandomMotds.get(r.nextInt(Config.Config.RandomMotds.size()));
            count = Integer.valueOf(count.intValue() + 1);
          }
        }
        count = Integer.valueOf(0);
        if (Message.contains("%player%")) {
          Message = "";
        }
        event.setResponse(new ServerPing(Version2, OnlinePlayers, String.valueOf(Config.Config.DefaultMotd.replace("&", "§")) + ChatColor.RESET + NewLine + ChatColor.RESET + Message.replace("&", "§"), ServerImage));
      }
    }
  }
}
