package de.MineX1991.ProxyManager.MOTD;

import java.util.ArrayList;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class Command_Motd
  extends Command
{
  public Command_Motd(String string)
  {
    super(string);
  }
  
  public void execute(CommandSender sender, String[] args)
  {
    if (sender.hasPermission("bungeecord.command.motd") || sender.hasPermission("bungeecord.*"))
    {
      if (args.length >= 1)
      {
        if (args[0].equalsIgnoreCase("list"))
        {
          sender.sendMessage(ChatColor.DARK_AQUA + "----------" + ChatColor.GOLD + "Motds" + ChatColor.DARK_AQUA + "----------");
          sender.sendMessage(ChatColor.BLUE + "Default-Motd: " + ChatColor.GRAY + Config.Config.DefaultMotd.replace("&", "§"));
          sender.sendMessage("");
          String OnlinePlayers = "";
          Integer count = Integer.valueOf(1);
          for (String s : Config.Config.RandomMotds) {
            if (OnlinePlayers.replace(" ", "") == "")
            {
              sender.sendMessage(count + ": " + ChatColor.GRAY + s.replace("&", "§"));
              count = Integer.valueOf(count.intValue() + 1);
            }
            else
            {
              sender.sendMessage(count + ": " + ChatColor.GRAY + s.replace("&", "§"));
              count = Integer.valueOf(count.intValue() + 1);
            }
          }
          if (count.intValue() == 1) {
            sender.sendMessage("-");
          }
          return;
        }
        if (args[0].equalsIgnoreCase("clear"))
        {
          Integer i = Integer.valueOf(Config.Config.RandomMotds.size());
          Config.Config.RandomMotds.clear();
          if (!Config.SaveConfig())
          {
            sender.sendMessage(ChatColor.RED + "Fehler: java.lang.file_write_error!");
            return;
          }
          Listener_Motds.LoadMotds();
          sender.sendMessage(i + " Random-Motds wurden erfolgreich gelöscht!");
          return;
        }
      }
      if (args.length <= 1)
      {
        sender.sendMessage(ChatColor.RED + "/motd setdefault <motd>");
        sender.sendMessage(ChatColor.RED + "/motd setnewbie <motd>");
        sender.sendMessage(ChatColor.RED + "/motd add <motd>");
        sender.sendMessage(ChatColor.RED + "/motd del <motd>");
        sender.sendMessage(ChatColor.RED + "/motd clear");
        sender.sendMessage(ChatColor.RED + "/motd list");
        return;
      }
      if (args.length >= 2)
      {
        if (args[0].equalsIgnoreCase("setdefault"))
        {
          String Message = "";
          Integer count = Integer.valueOf(1);
          while (count.intValue() < args.length)
          {
            Message = Message != "" ? String.valueOf(Message) + " " + args[count.intValue()] : args[count.intValue()];
            count = Integer.valueOf(count.intValue() + 1);
          }
          Config.Config.DefaultMotd = Message;
          if (!Config.SaveConfig())
          {
            sender.sendMessage(ChatColor.RED + "Fehler: java.lang.file_write_error!");
            return;
          }
          Listener_Motds.LoadMotds();
          sender.sendMessage(ChatColor.GOLD + "Die Default-Motd wurde erfolgreich zu \"" + ChatColor.GRAY + Message.replace("&", "§") + ChatColor.GOLD + "\" geändert!");
          return;
        }
        if (args[0].equalsIgnoreCase("add"))
        {
          String Message = "";
          Integer count = Integer.valueOf(1);
          while (count.intValue() < args.length)
          {
            Message = Message != "" ? String.valueOf(Message) + " " + args[count.intValue()] : args[count.intValue()];
            count = Integer.valueOf(count.intValue() + 1);
          }
          Config.Config.RandomMotds.add(Message);
          if (!Config.SaveConfig())
          {
            sender.sendMessage(ChatColor.RED + "Fehler: java.lang.file_write_error!");
            return;
          }
          Listener_Motds.LoadMotds();
          sender.sendMessage(ChatColor.GOLD + "Die Motd \"" + ChatColor.GRAY + Message.replace("&", "§") + ChatColor.GOLD + "\" wurde erfolgreich hinzugefügt!");
          return;
        }
        if ((args[0].equalsIgnoreCase("remove")) || (args[0].equalsIgnoreCase("del")))
        {
          Integer remove = Integer.valueOf(0);
          try
          {
            remove = Integer.valueOf(args[1]);
            if (remove.intValue() <= 0)
            {
              sender.sendMessage(ChatColor.RED + "/motd del <zahl 1-" + Config.Config.RandomMotds.size() + ">");
              return;
            }
            if (remove.intValue() > Config.Config.RandomMotds.size())
            {
              sender.sendMessage(ChatColor.RED + "/motd del <zahl 1-" + Config.Config.RandomMotds.size() + ">");
              return;
            }
          }
          catch (Exception e)
          {
            sender.sendMessage(ChatColor.RED + "/motd del <zahl>");
            return;
          }
          if (Config.Config.RandomMotds.size() != 0)
          {
            sender.sendMessage(ChatColor.GOLD + "Die Motd \"" + ChatColor.GRAY + ((String)Config.Config.RandomMotds.get(remove.intValue() - 1)).replace("&", "§") + ChatColor.GOLD + "\" wurde erfolgreich gel§scht!");
            Config.Config.RandomMotds.remove(remove.intValue() - 1);
            if (!Config.SaveConfig())
            {
              sender.sendMessage(ChatColor.RED + "Fehler: java.lang.file_write_error!");
              return;
            }
            Listener_Motds.LoadMotds();
          }
          else
          {
            sender.sendMessage(ChatColor.RED + "/motd del <zahl 0-" + Config.Config.RandomMotds.size() + ">");
          }
          return;
        }
        sender.sendMessage(ChatColor.RED + "/motd setdefault <motd>");
        sender.sendMessage(ChatColor.RED + "/motd setnewbie <motd>");
        sender.sendMessage(ChatColor.RED + "/motd add <motd>");
        sender.sendMessage(ChatColor.RED + "/motd del <zahl>");
        sender.sendMessage(ChatColor.RED + "/motd clear");
        sender.sendMessage(ChatColor.RED + "/motd list");
      }
    }
    else
    {
      sender.sendMessage(ChatColor.RED + "Keine Rechte!");
    }
  }
}
