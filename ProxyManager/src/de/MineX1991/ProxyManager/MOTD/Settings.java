package de.MineX1991.ProxyManager.MOTD;

import de.MineX1991.ProxyManager.Main.Data;
import java.io.File;
import java.util.ArrayList;
import net.cubespace.Yamler.Config.Config;
import net.cubespace.Yamler.Config.InvalidConfigurationException;
import net.md_5.bungee.api.plugin.Plugin;

public class Settings
  extends Config
{
  public Integer MaxPlayers = Integer.valueOf(1);
  public Boolean isLocked = Boolean.valueOf(false);
  public Boolean noIPs = Boolean.valueOf(true);
  public Boolean AutoMessagesEnabled = Boolean.valueOf(false);
  public String AutoMessagesPrefix = "&3[&bAutoMessager&3]&7";
  public String Prefix = Data.prefix;
  public ArrayList<String> AutoMessages = new ArrayList();
  public Boolean useMotds = Boolean.valueOf(true);
  public String DefaultMotd = "&cTryRusher.net ";
  public ArrayList<String> RandomMotds = new ArrayList();
  public ArrayList<String> MutedPlayers = new ArrayList();
  public ArrayList<String> MutedWerbung = new ArrayList();
  public ArrayList<String> Werbungsserver = new ArrayList();
  public Integer MaxWarns = Integer.valueOf(8);
  public ArrayList<String> PlayerWarns = new ArrayList();
  public ArrayList<String> BannedPlayers = new ArrayList();
  public Integer TimeBanDays = Integer.valueOf(0);
  
  public Settings(Plugin plugin)
  {
    this.CONFIG_HEADER = new String[] { "Einstellungen für System" };
    this.CONFIG_FILE = new File(plugin.getDataFolder(), "einstellungen.yml");
    try
    {
      init();
    }
    catch (InvalidConfigurationException ex)
    {
      ex.printStackTrace();
    }
  }
}
