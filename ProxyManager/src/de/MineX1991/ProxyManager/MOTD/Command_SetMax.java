package de.MineX1991.ProxyManager.MOTD;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class Command_SetMax
  extends Command
{
  public Command_SetMax(String string)
  {
    super(string);
  }
  
  public void execute(CommandSender sender, String[] args)
  {
    if (sender.hasPermission("bungeecord.command.setmax") || sender.hasPermission("bungeecord.*"))
    {
      if (args.length == 1) {
        try
        {
          Config.Config.MaxPlayers = Integer.valueOf(args[0]);
          if (!Config.SaveConfig())
          {
            sender.sendMessage(ChatColor.RED + "Fehler: java.lang.file_write_error!");
            return;
          }
          sender.sendMessage(ChatColor.GOLD + "§7Die maximale §bSpieleranzahl§7 wurde zu §b" + args[0] + ChatColor.GOLD + "§7 gesetzt!");
        }
        catch (Exception ex)
        {
          sender.sendMessage(ChatColor.RED + "§b/setmax [maxplayers]");
        }
      } else {
        sender.sendMessage(ChatColor.GOLD + "§7Es können maximal §b" + Config.Config.MaxPlayers + ChatColor.GOLD + "§7 Spieler online!");
      }
    }
    else {
      sender.sendMessage("§cKeine Rechte.");
    }
  }
}
